const users = [];

// Join user to chat
function userJoin(id, username, room, idccms) {
  const user = { id, username, room, idccms };

  users.push(user);
  
}

// User leaves chat
function userLeave(id) {
  const index = users.findIndex(user => user.id === id);

  const temporal = users.filter(data => {return data.id == id})
  
  if (index !== -1) {
   users.splice(index, 1)[0];
  }
  return temporal;
}

// Get room users
function getRoomUsers(room) {
  return users.filter(user => user.room === room);
}

function getIdccms(idccms) {
  const index = users.filter(user => user.idccms === idccms);
  if (index[0]){
    userLeave(index.id);
    return index[0];
  }
  return false;
}

module.exports = {
  userJoin,
  userLeave,
  getRoomUsers,
  getIdccms
};
