const mongoose = require("./config"),
  Schema = mongoose.Schema;

const schemas = {
  centralSchema: new Schema({
    id_: Schema.Types.ObjectId,
    central: String,
  }),
  userSchema: new Schema({
    name: String,
    idccms: Number,
    area: String,
    rol: String,
    email: String,
  }),
  transferSchema: new Schema({
    oldArea: String,
    newArea: String,
    idRequest: Number,
  }),
  taskSchema: new Schema({
    id_: Schema.Types.ObjectId,
    idccms: Number,
    task: String,
    description: String,
    priority: Number,
    dataTime: Date,
    status: Number,
  }),
  caseSchema: new Schema({
    id_: Schema.Types.ObjectId,
    case: String,
    area: String,
    project: String,
    priority: Number,
    time: Number,
    timeType: Number,
    dataTime: String,
  }),
  subProjectSchema: new Schema({
    id_: Schema.Types.ObjectId,
    name: String,
    area: String,
  }),
  projectSchema: new Schema({
    id_: Schema.Types.ObjectId,
    name: String,
  }),
  requestSchema: new Schema(
    {
      id_: Schema.Types.ObjectId,
      idrequest: Number,
      idccms: Number,
      areaResponsable: String,
      responsable: Number,
      createPerson: String,
      project: String,
      status: String,
      trackingStatus: [{ status: Number, message: String }],
      creationDateTime: Date,
      finishDate: Date,
      client: String,
      email: String,
      cellphone: String,
      priority: Number,
      type: String,
      problem: String,
      dateTransfer: Date,
      areaTransfer: String,
      closedPerson: String,
      closedDatatime: Date,
      closed: String,
      dataTimeEstimation: String,
      fileClosed: {
        name: String,
        idkey: String,
        path: String,
        size: Number,
        type: String,
      },
      files: [
        {
          name: String,
          idkey: String,
          path: String,
          size: Number,
          type: String,
        },
      ],
    },
    { typeKey: "$type" }
  ),
};

const models = {
  request: mongoose.model("requests", schemas.requestSchema),
  central: mongoose.model("centrals", schemas.centralSchema),
  user: mongoose.model("users", schemas.userSchema),
  transfer: mongoose.model("transfers", schemas.transferSchema),
  case: mongoose.model("cases", schemas.caseSchema),
  project: mongoose.model("projects", schemas.projectSchema),
  subProject: mongoose.model("subprojects", schemas.subProjectSchema),
  task: mongoose.model("tasks", schemas.taskSchema),
};

module.exports = models;
