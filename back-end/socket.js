const db = require("./mongo/models");
var io;
let startSocket = (server) => {
  io = require("socket.io").listen(server);
  
  io.of('/graficas').on("connection", (socket) => {
    socket.on("joinRoom", (room) => {
      socket.join(room);
      console.log(`Estas conectado a la room ${room} tu id es ${socket.id}`);
      io.in(room).emit("success", "Bienvenido");
    });
    socket.on("error", (data) => {
      console.log(data);
    });
  });
};

async function getRequestIdccms(idccms) {
  let filtre = {
    idccms: idccms,
    status: "opend",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => io.in(idccms).emit(`requests`, result));
}
async function getRequestDevs(idccms,area) {
  if (idccms != 0) {
    let filter = [
      {
        responsable: idccms,
        status: "opend",
      },
      {
        idccms: idccms,
        status: "opend",
      },
    ];
    db.request
      .find({ $or: filter })
      .sort({ idrequest: -1 })
      .then((result) => io.in(area).emit(`requestsDev`, result));
  }
}
async function getRequestAdmins(area) {
  let filtre = {
    areaResponsable: area,
    status: "opend",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => io.in(area).emit(`requestsAdmin`, result));
}

async function getDataGraf(area) {
  let msgGrafic = {
    incoming: 0,
    solved: 0,
  };
  let resp = await db.request
    .find({ status: "closed", areaResponsable: area })
    .count()
    .then((data) => {
      msgGrafic.solved = data;
    });
  let resp2 = await db.request
    .find({ status: "opend", areaResponsable: area })
    .count()
    .then((data) => {
      msgGrafic.incoming = data;
    });

  io.in(area).emit(`grafico`, [
    {
      name: "Pendientes",
      value: msgGrafic.incoming,
    },
    {
      name: "Solucionados",
      value: msgGrafic.solved,
    },
  ]);
}

module.exports = {
  startSocket,
  getDataGraf,
  getRequestIdccms,
  getRequestDevs,
  getRequestAdmins,
};
