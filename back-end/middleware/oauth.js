require("dotenv").config();
const redirect = require("../controllers/redirect.controller");
const sql = require("../controllers/sql.controller");
const parametros = require("../controllers/params.controller").parametros;
const db = require("../mongo/models");

const url = "https://oauth.teleperformance.co/api/";

function GetInfo(idccms,name,username) {
  return new Promise((resolve, reject) => {
    sql
      .query("SpTPS_InsertUser", parametros({idccms:idccms,name:name, user: username}, "SpTPS_InsertUser"))
      .then((save) => {
        console.log(save,'insert user');
        sql
          .query("SpTPS_Login", parametros(idccms, "SpTPS_Login"))
          .then((resultado) => {
            try {
              console.log(resultado);
              resolve({
                rol: resultado.Result[0].Rol,
                area: resultado.Result[0].Area,
                email: resultado.Result[0].email,
              });
            } catch (error) {
              resolve(null);
            }
          })
          .catch((err) => {
            {
              reject(err);
            }
          });
      });
  });
}

function LoginResp(req, res) {
  return new Promise((resolve, reject) => {
    let data = {
      body: req.body.body,
      project: process.env.PROJECT,
      ip: req.clientIp,
      timeTkn: 3650000,
      uri: req.originalUrl,
      size: req.headers["content-length"],
    };
    redirect
      .post(url, "oauthlogin", data, null)
      .then((result) => {
        resolve(result.data.data);
      })
      .catch((error) => {
        responsep(3, req, res, error);
      });
  });
}

async function login(req, res) {
  let result = await LoginResp(req, res);
  console.log(result);
  let getInfo = await GetInfo(result.idccms, result.nombre,result.username);
  if (getInfo != null) {
    result.rol = getInfo.rol;
    result.area = getInfo.area;
    result.email = getInfo.email;
  } else {
    result.area = "ALL";
  }
  res.cookie(req.csrfToken());
  responsep(1, req, res, result);
}

function refresh(req, res) {
  let data = {
    body: req.body,
    project: process.env.PROJECT,
    ip: req.clientIp,
    uri: req.originalUrl,
    size: req.headers["content-length"],
  };
  redirect
    .post(url, "oauthrefresh", data, req.headers.authorization.split(" ")[1])
    .then((result) => {
      console.log(result);
      responsep(1, req, res, result.data.data);
    })
    .catch((error) => {
      console.log(error);
      responsep(3, req, res, error);
    });
}

let responsep = (tipo, req, res, resultado) => {
  return new Promise((resolve, reject) => {
    let date = new Date();
    if (tipo == 1) {
      res.status(200).json(resultado);
      resolve(200);
    } else if (tipo == 2) {
      console.log("Error at:", date, "res: ", resultado.message);
      res.status(404).json(resultado.message);
      resolve(404);
    } else if (tipo == 3) {
      res.status(401).json(resultado);
      resolve(401);
    }
  });
};

function oauthOther(req, res, next) {
  let data = {
    project: process.env.PROJECT,
    ip: req.clientIp,
    uri: req.originalUrl,
    size: req.headers["content-length"],
  };
  redirect
    .post(url, "oauthothers", data, req.headers.authorization.split(" ")[1])
    .then((result) => {
      if (result.status == 200) {
        next();
      }
    })
    .catch((error) => {
      console.log(error);
      responsep(2, req, res, error);
    });
}

module.exports = { oauthOther, refresh, login };