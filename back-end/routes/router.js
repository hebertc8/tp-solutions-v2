const routes = require("../controllers/routes.controller");
const sqlRoutes = require("../controllers/routesSql.controller");
const oauth = require("../middleware/oauth");

const multipart = require("connect-multiparty");
const multipartMiddleware = multipart({
  uploadDir: "./uploads",
});

module.exports = (router) => {
  //Login
  router.post("/ccmslogin", (req, res) => {
    oauth.login(req, res);
  });

  //Refresh token
  router.post("/refreshToken", (req, res) => {
    oauth.refresh(req, res);
  });
  
  //router.get("/download", routes.downloadBill); spTPS_GetFileListInfo
  //router.get("/downloadClosed", routes.downloadBillClosed);
/*   router.post("/addtask", routes.addTask);
  router.post("/finishTask", routes.finishTask);
  router.post("/getTask", routes.getTask);
  router.post("/getfinishTask", routes.getfinishTask);
   */
 
  
  
  

  router.post("/cancelRequest", sqlRoutes.cancelRequest);
  router.post("/addRequest", multipartMiddleware, (req, res) => {
    sqlRoutes.addRequest(req, res);
  });
  router.post("/closeRequest", multipartMiddleware, (req, res) => {
    sqlRoutes.closeRequest(req, res);
  });
  router.post("/transferArea", sqlRoutes.transferArea);
  router.post("/transferPerson", sqlRoutes.transferPerson);
  router.post("/updateEmail", routes.updateEmail);
  router.post("/getInfo", sqlRoutes.getInfo);
  router.get("/download", sqlRoutes.downloadBill); 
  MapSpRouterSQL("/deleteCase", 'spTPS_DeactivateIncidence');
  MapSpRouterSQL("/getReport",'SpTPS_Report');
  MapSpRouterSQL("/updateEmail", "spTPS_UpdateUserEmail");
  MapSpRouterSQL("/addCase", "spTPS_NewIncidence");
  MapSpRouterSQL("/getRequest", "SpTPS_ClosedArea");
  MapSpRouterSQL("/getRequestById", "SpTPS_ClosedRequest");
  MapSpRouterSQL("/getRequestByResponsable", "SpTPS_ClosedResponsable");
  MapSpRouterSQL("/getRequestRolAdmin", "SpTPS_OpenArea");
  MapSpRouterSQL("/getRequestByIdOpen", "SpTPS_OpenRequest");
  MapSpRouterSQL("/getRequestRol", "SpTPS_OpenResponsable");
  MapSpRouterSQL("/basicGraft", "spTPS_GetGraphicData");
  MapSpRouterSQL("/survey","spTPS_InsertarEncuestaSatisfaccion");  
  //CHAT
  // MapSpRouter("/getIncidents", "spGetIncidents");
  MapSpRouter("/getListRooms", "spTPChat_GetListRooms");
  MapSpRouter("/getListConversation", "spTPChat_GetListConversation");
  // MapSpRouter("/getResponse", "spGetResponse");
  MapSpRouter("/getTrackConversation", "spTPChat_GetTrackConversation");
  MapSpRouter("/getListConversationAdmin", "spTPChat_GetListConversationByUserAdmin");
  MapSpRouter("/getListConversationAgent", "spTPChat_GetListConversationByUserAgent");
  MapSpRouter("/login", "spTPChatLogin");
  MapSpRouter("/setConversation", "spTPChat_SetConversation");
  MapSpRouter("/setTrackConversation", "spTPChatSetTrackConversation");
  MapSpRouter("/updateConversation", "spTPChat_UpdateConversations");
  MapSpRouter("/getConversationFact", "spTPChat_GetInfoConversationFact");
  MapSpRouter("/getEmailUser", "spTPS_GetEmailUser");
  // MapSpRouter("/setAgentInfo", "spSetAgentInfo");
  // MapSpRouter("/getListCity", "spGetLisCities");

  function MapSpRouterSQL(route, spName) {
    router.post(route, oauth.oauthOther, (req, res) =>
      sqlRoutes.CallSp(spName, req, res)
    );
  }
  function MapSpRouter(route, spName) {
    router.post(route, oauth.oauthOther, (req, res) =>
      routes.CallSp(spName, req, res)
    );
  }
};