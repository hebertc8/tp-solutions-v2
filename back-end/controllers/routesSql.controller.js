const sql = require("./sql.controller");
const parametros = require("./params.controller").parametros;
const config = require("../properties/properties").valor;
const jwt = require("jsonwebtoken");
const { decrypt } = require("./crypt.controller");
const { transporter } = require("../mailer/mailer");
const sockets = require("../socket.js");
var moment = require("moment");

let getDataToken = (req, res) => {
  return new Promise((resolve, reject) => {
    try {
      let payload = jwt.verify(
        req.headers.authorization.split(" ")[1],
        config.secret
      );
      let data = JSON.parse(decrypt(payload.data));
      resolve(data);
    } catch (error) {
      res.status(401).json({ message: "UnauthorizedError", code: 400 });
    }
  });
};

async function downloadBill(req, res) {
  console.log(req.query.doc, "idfact");
  sql
    .query(
      "spTPS_GetFileListInfo",
      parametros({ id: req.query.doc }, "spTPS_GetFileListInfo")
    )
    .then((resultado) => {
      let file = `${process.cwd()}/${resultado.Result[0].path}`;
      res.download(file, resultado.Result[0].name);
    })
    .catch((err) => {
      {
        console.log(err);
        responsep(2, req, res, err);
      }
    });
}

async function getInfo(req, res) {
  let consult = {
    centrals: [],
    project: [],
    areaWorkers: [],
    cases: [],
    subProject: [],
  };
  let centrals = await sql
    .query(
      "SpTPS_Generales",
      parametros({ area: req.body.area, types: 1 }, "SpTPS_Generales")
    )
    .then((resultado) => {
      consult.centrals = resultado.Result;
    })
    .catch((err) => {
      {
        reject(err);
      }
    });
  let project = await sql
    .query(
      "SpTPS_Generales",
      parametros({ area: req.body.area, types: 2 }, "SpTPS_Generales")
    )
    .then((resultado) => {
      consult.project = resultado.Result;
    })
    .catch((err) => {
      {
        reject(err);
      }
    });
  let cases = await sql
    .query(
      "SpTPS_Generales",
      parametros({ area: req.body.area, types: 5 }, "SpTPS_Generales")
    )
    .then((resultado) => {
      consult.cases = resultado.Result;
    })
    .catch((err) => {
      {
        reject(err);
      }
    });
  let areaWorkers = await sql
    .query(
      "SpTPS_Generales",
      parametros({ area: req.body.area, types: 4 }, "SpTPS_Generales")
    )
    .then((resultado) => {
      consult.areaWorkers = resultado.Result;
    })
    .catch((err) => {
      {
        reject(err);
      }
    });
  let subProjects = await sql
    .query(
      "SpTPS_Generales",
      parametros({ area: req.body.area, types: 3 }, "SpTPS_Generales")
    )
    .then((resultado) => {
      consult.subProject = resultado.Result;
    })
    .catch((err) => {
      {
        reject(err);
      }
    });
  responsep(1, req, res, consult);
}

async function addRequest(req, res) {
  try {
    const payload = await getDataToken(req, res);
    console.log(req.body);
    let dataQuery = {
      idccms: payload.data.idccms,
      central: Number(req.body.client),
      descripcion: req.body.problem,
      dataPromise: req.body.finishDate,
      incidence: Number(req.body.type),
      area: Number(req.body.areaResponsable),
      email: req.body.email,
      telefono: req.body.cellphone,
    };
    console.log(dataQuery);
    let resultado = { id: 1, Incidencia: 22, problem: "test" };

    sql
      .query("SpTPS_NewRequest", parametros(dataQuery, "SpTPS_NewRequest"))
      .then((resultado) => {
        let result = resultado.Result[0];
        result.problem = req.body.problem;
        console.log(result, "datos retornados.");
        let name = "";
        let path = "";
        let size = "";
        let type = "";

        if (req.files.files) {
          if (req.files.files.length > 0) {
            let time = 0;
            dataFiles = req.files.files;
            dataFiles.forEach((element) => {
              if (time == dataFiles.length - 1) {
                name = name + element.name;
                path = path + element.path.replace("\\", "/");
                size = size + element.size.toString();
                type = type + element.type;
              } else {
                time += 1;
                name = name + element.name + ",";
                path = path + element.path.replace("\\", "/") + ",";
                size = size + element.size.toString() + ",";
                type = type + element.type + ",";
              }
            });
          } else {
            name = req.files.files.name;
            path = req.files.files.path.replace("\\", "/");
            size = req.files.files.size.toString();
            type = req.files.files.type;
          }
        }
        if (resultado != 400) {
          let id = resultado.Result[0].id;
          let dataFil = {
            id: resultado.Result[0].id,
            name: name,
            path: path,
            size: size,
            type: type,
          };
          if (name != "") {
            sql
              .query(
                "SpTPS_InsertFiles",
                parametros(dataFil, "SpTPS_InsertFiles")
              )
              .then((resultado) => {
                console.log(resultado, "resultado");
                SendEmail(
                  req.body.areaResponsable,
                  "Nueva Solicitud",
                  1,
                  result
                );
                sockets.getDataGraf(req.body.areaResponsable);
                responsep(1, req, res, id);
              });
          } else {
            SendEmail(req.body.areaResponsable, "Nueva Solicitud", 1, result);
            responsep(1, req, res, id);
          }
        } else {
          responsep(2, req, res, 400);
        }
      })
      .catch((err) => {
        {
          console.log(err);
          responsep(2, req, res, err);
        }
      });
  } catch (err) {
    console.log(err);
    responsep(2, req, res, err);
  }
}

async function cancelRequest(req, res) {
  const payload = await getDataToken(req, res);
  sql
    .query(
      "spTPS_CancelRequest",
      parametros(
        { fact_id: req.body._id, user_id: payload.data.idccms },
        "spTPS_CancelRequest"
      )
    )
    .then((resultado) => {
      /* sockets.getDataGraf(req.body.area); */
      /* sockets.getRequestAdmins(req.body.area); */
      responsep(1, req, res, "Cancel successful");
    })
    .catch((err) => {
      {
        responsep(2, req, res, err);
      }
    });
}

async function transferArea(req, res) {
  const payload = await getDataToken(req, res);
  sql
    .query(
      "spTPS_TransferRequestArea",
      parametros(
        {
          fact_id: Number(req.body._id),
          newArea: Number(req.body.newArea),
          motive: req.body.motive,
          user_id: payload.data.idccms,
        },
        "spTPS_TransferRequestArea"
      )
    )
    .then((result) => {
      let dataSend = {
        motive: req.body.motive,
        problem: req.body.problem,
        type: req.body.type,
        idrequest: req.body._id,
      };
      /* sockets.getRequestIdccms(result.idccms); */
      /* sockets.getRequestAdmins(req.body.areanew); */
      /* sockets.getRequestAdmins(req.body.areaold); */
      SendEmail(
        req.body.newArea,
        "Transladaron a tu area una solicitud",
        2,
        dataSend
      );
      responsep(1, req, res, "Successful transfer");
    })
    .catch((err) => {
      {
        responsep(2, req, res, err);
      }
    });
}

async function ValideAreaPerson(idccms, area) {
  return new Promise((resolve, reject) => {
    sql
      .query(
        "spTPS_ValidatePersonByArea",
        parametros({ idccms: idccms, area: area }, "spTPS_ValidatePersonByArea")
      )
      .then((result) => {
        if (result != null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch((err) => {
        {
          console.log(result);
          reject(err);
        }
      });
  });
}

async function transferPerson(req, res) {
  const validePerson = await ValideAreaPerson(
    req.body.idccmsResp,
    req.body.area
  );
  if (validePerson) {
    const payload = await getDataToken(req, res);
    sql
      .query(
        "spTPS_TransferRequest",
        parametros(
          {
            fact_id: req.body._id,
            IdNewResponsable: req.body.idccmsResp,
            Reporter_id: payload.data.idccms,
          },
          "spTPS_TransferRequest"
        )
      )
      .then((result) => {
        /* sockets.getRequestIdccms(req.body.idccms); */
        /* sockets.getRequestDevs(req.body.idccmsResp, req.body.area); */
        /* sockets.getRequestAdmins(req.body.area); */
        let dataEmail = {
          problem: req.body.problem,
          type: req.body.type,
          idrequest: req.body._id,
        };
        console.log(dataEmail);
        SendEmail(result[0].email, "Te asignaron una solicitud.", 3, dataEmail);
        responsep(1, req, res, "Successful transfer");
      })
      .catch((err) => {
        {
          responsep(2, req, res, err);
        }
      });
  } else {
    responsep(2, req, res, "The staff does not belong to your area");
  }
}

async function getEmailsAdmin(area) {
  return new Promise((resolve, reject) => {
    let emails = "";
    sql
      .query(
        "spTPS_GetEmailsUserAdminByArea",
        parametros({ area: area }, "spTPS_GetEmailsUserAdminByArea")
      )
      .then((result) => {
        for (let i = 0; i < result.Result.length; i++) {
          if (i == result.Result.length - 1) {
            emails = emails + result.Result[i].email;
          } else {
            emails = emails + result.Result[i].email + ", ";
          }
        }
        resolve(emails);
      })
      .catch((err) => {
        {
          responsep(2, req, res, err);
        }
      });
  });
}

async function SendEmail(emails, subject, type, reqst) {
  let html = "";
  switch (type) {
    case 1:
      html = `<b>Nueva Solicitud</b>
      <p><strong>Numero de referencia: </strong> ${reqst.id}</p>
      <p><strong>Tipo : </strong> ${reqst.Incidencia}</p>
      <p><strong>Descripcion : </strong> ${reqst.problem}</p>
      <p>por favor revisa TP soluccion realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;
      let respL = await getEmailsAdmin(emails).then((res) => {
        emails = res;
      });
      break;
    case 2:
      html = `<b>Translado de area</b>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Tipo : </strong> ${reqst.type}</p>
      <p><strong>Descripcion : </strong> ${reqst.problem}</p>
        <p><strong>Motivo : </strong> ${reqst.motive}</p>
      <p>por favor revisa TP soluccion realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;

      let respS = await getEmailsAdmin(emails).then((res) => {
        emails = res;
      });
      let respU = await sql
        .query(
          "spTPS_GetEmailRequestUser",
          parametros({ fact_id: reqst.idrequest }, "spTPS_GetEmailRequestUser")
        )
        .then((result) => {
          console.log(result.Result[0].email);
          emails = emails + ", " + result.Result[0].email;
        });
      break;
    case 3:
      html = `<b>Te asignaron una solicitud</b>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Tipo : </strong> ${reqst.type}</p>
      <p><strong>Descripcion : </strong> ${reqst.problem}</p>
      <p>
        por favor revisa TP soluccion realiza click
        <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a>
      </p>`;
      break;
    case 4:
      html = `<h1>Tu solicitud fue solucionada</h1>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Solucion : </strong> ${reqst.closed}</p>
      <br/>
      <p>Te invitamos a llenar nuestra <a href="https://tpsolutionsdemo.teleperformance.co/survey/${reqst.idrequest}">Encuesta de satisfaccion</a>, gracias</p>
      <br/>
      <p>por favor revisa TP soluccion en la seccion de my historic, realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;
      let resp = await sql
        .query(
          "spTPS_GetEmailRequestUser",
          parametros({ fact_id: reqst.idrequest }, "spTPS_GetEmailRequestUser")
        )
        .then((result) => {
          emails = result.Result[0].email;
        });
      break;
    case 5:
      html = `<h1>Tienes un nuevo mensaje sin leer en TP Solution</h1>
        <p><strong>Numero del ticket: </strong> ${reqst}</p>
      <p>por favor revisa TP soluccion en la seccion de chat, realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;
      break;

    default:
      break;
  }
  let info = await transporter.sendMail({
    from: '"TP Solution V2" <TpSolutions@teleperformance.com>',
    to: emails,
    subject: subject,
    html: html,
  });
}

async function closeRequest(req, res) {
  const payload = await getDataToken(req, res);
  let dataSolverd = {};
  if (req.files.files) {
    dataSolverd = {
      fact_id: req.body._id,
      reporter_id: payload.data.idccms,
      ClosedDescription: req.body.closed,
      nameDoc: req.files.files.name,
      pathDoc: req.files.files.path.replace("\\", "/"),
      sizeDoc: req.files.files.size,
      typeDoc: req.files.files.type,
    };
  } else {
    dataSolverd = {
      fact_id: req.body._id,
      reporter_id: payload.data.idccms,
      ClosedDescription: req.body.closed,
    };
  }
  sql
    .query("spTPS_SolveRequest", parametros(dataSolverd, "spTPS_SolveRequest"))
    .then((result) => {
      let dataEmail = { idrequest: req.body._id, closed: req.body.closed };
      SendEmail(req.body._id, "Tu solicitud fue solucionada", 4, dataEmail);
      /* sockets.getDataGraf(req.body.area); 
      /* sockets.getRequestAdmins(req.body.area); */
      responsep(1, req, res, "Updated successful");
    })
    .catch((err) => {
      {
        responsep(2, req, res, err);
      }
    });
}

async function CallSp(spName, req, res) {
  if (isEmpty(req.body)) {
    const payload = await getDataToken(req, res);
    sql
      .query(spName, parametros(payload.data.idccms, spName))
      .then((resultado) => {
        responsep(1, req, res, resultado);
      })
      .catch((err) => {
        {
          responsep(2, req, res, err);
        }
      });
  } else {
    sql
      .query(spName, parametros(req.body, spName))
      .then((result) => {
        responsep(1, req, res, result);
      })
      .catch((err) => {
        responsep(2, req, res, err);
      });
  }
}

function isEmpty(req) {
  for (var key in req) {
    if (req.hasOwnProperty(key)) return false;
  }
  return true;
}

let responsep = (tipo, req, res, resultado, cookie) => {
  return new Promise((resolve, reject) => {
    if (tipo == 1) {
      res.cookie("XSRF-TOKEN", req.csrfToken(), {
        "max-Age": new Date(Date.now() + 90000000),
        path: "/",
      });
      res.status(200).json(resultado);
    } else if (tipo == 2) {
      res.status(400).json(resultado);
    }
  });
};

module.exports = {
  getInfo,
  addRequest,
  downloadBill,
  cancelRequest,
  transferArea,
  transferPerson,
  SendEmail,
  closeRequest,
  CallSp,
};
