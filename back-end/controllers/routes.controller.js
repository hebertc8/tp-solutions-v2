const db = require("../mongo/models");
const sockets = require("../socket.js");
const config = require("../properties/properties").valor;
const sql = require("./sql.controller");
const parametros = require("./params.controller").parametros;
const jwt = require("jsonwebtoken");
const { decrypt } = require("./crypt.controller");
const { transporter } = require("../mailer/mailer");
const socket = require("../socket.js");
const { json } = require("body-parser");

let getDataToken = (req, res) => {
  return new Promise((resolve, reject) => {
    try {
      let payload = jwt.verify(
        req.headers.authorization.split(" ")[1],
        config.secret
      );
      let data = JSON.parse(decrypt(payload.data));
      resolve(data);
    } catch (error) {
      res.status(401).json({ message: "UnauthorizedError", code: 400 });
    }
  });
};

let CallSp = (spName, req, res) => {
  // const payload = jwt.verify(req.headers.authorization.split(" ")[1],  process.env.SECRET);
  // const idccms = JSON.parse(decrypt(payload.data)).data.idccms;
  if (isEmpty(req.body)) {

    sql
      .query(spName, null)
      .then((resultado) => {
        responsep(1, req, res, resultado);
      })
      .catch((err) => {
        {
          responsep(2, req, res, err);
        }
      });
  } else {
    sql
      .query(spName, parametros(req.body, spName))
      .then((result) => {
        responsep(1, req, res, result);
      })
      .catch((err) => {
        responsep(2, req, res, err);
      });
  }
};



function isEmpty(req) {
  for (var key in req) {
    if (req.hasOwnProperty(key))
      return false;
  }
  return true;
}

let getIdRequest = () => {
  return new Promise((resolve) => {
    db.request
      .find()
      .countDocuments()
      .then((res) => {
        resolve(res + 1);
      });
  });
};

async function getInfo(req, res) {
  let consult = {
    centrals: [],
    project: [],
    areaWorkers: [],
    cases: [],
    subProject: [],
  };
  let centrals = await db.central
    .find({}, { central: 1, _id: 0 })
    .then((data) => {
      consult.centrals = data;
    });
  let project = await db.project.find({}, { _id: 0 }).then((data) => {
    consult.project = data;
  });
  let cases = await db.case.find({}).then((data) => {
    consult.cases = data;
  });
  let areaWorkers = await db.user
    .find({ area: req.body.area }, { _id: 0, area: 0, rol: 0 })
    .then((data) => {
      consult.areaWorkers = data;
    });
  let subProjects = await db.subProject.find({}, { _id: 0 }).then((data) => {
    consult.subProject = data;
  });
  responsep(1, req, res, consult);
}

async function addCase(req, res) {
  let query = new db.case({
    case: req.body.case.toUpperCase(),
    area: req.body.area,
    project: req.body.project,
    priority: req.body.priority,
    time: req.body.time,
    timeType: req.body.timeType,
    dataTime: req.body.dataTime,
  });
  query
    .save()
    .then((result) => {
      db.case.find({}).then((datafind) => {
        responsep(1, req, res, datafind);
      });
    })
    .catch((err) => {
      console.log(err);
      responsep(2, req, res, "Creation error");
    });
}

async function deleteCase(req, res) {
  db.case.findByIdAndDelete(req.body._id, function (err, docs) {
    if (err) {
      responsep(2, req, res, err);
    } else {
      db.case.find({}).then((datafind) => {
        responsep(1, req, res, datafind);
      });
    }
  });
}

async function addRequest(req, res) {
  try {
    const payload = await getDataToken(req, res);
    const temp = await getIdRequest();
    let dataFiles;
    let query = new db.request({
      idrequest: temp,
      idccms: payload.data.idccms,
      createPerson: payload.data.nombre,
      responsable: req.body.responsable,
      status: "opend",
      creationDateTime: Date.now(),
      project: req.body.project,
      areaResponsable: req.body.areaResponsable,
      client: req.body.client,
      email: req.body.email,
      cellphone: req.body.cellphone,
      priority: req.body.priority,
      finishDate: req.body.finishDate,
      type: req.body.type,
      problem: req.body.problem,
      dataTimeEstimation: req.body.dataTimeEstimation,
      trackingStatus: [],
      files: [],
      dateTransfer: null,
      areaTransfer: null,
      closedDatatime: null,
      closedPerson: null,
      closed: null,
    });
    query.trackingStatus.push({ status: 1, message: "In review" });
    if (req.files.files) {
      if (req.files.files.length > 0) {
        dataFiles = req.files.files;
        dataFiles.forEach((element) => {
          query.files.push({
            name: element.name,
            path: element.path.replace("\\", "/"),
            size: element.size,
            type: element.type,
          });
        });
      } else {
        query.files.push({
          name: req.files.files.name,
          path: req.files.files.path.replace("\\", "/"),
          size: req.files.files.size,
          type: req.files.files.type,
        });
      }
    }
    query
      .save()
      .then((result) => {
        SendEmail(req.body.areaResponsable, "Nueva Solicitud", 1, result);
        sockets.getDataGraf(req.body.areaResponsable);
        responsep(1, req, res, "Save successfully");
      })
      .catch((err) => {
        console.log(err);
        responsep(2, req, res, err);
      });
  } catch (err) {
    console.log(err);
    responsep(2, req, res, err);
  }
}

async function getRequestById(req, res) {
  const payload = await getDataToken(req, res);
  let filtre = {
    idccms: payload.data.idccms,
    status: "closed",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => responsep(1, req, res, result))
    .catch((err) => responsep(2, req, res, err));
}

async function getRequestByResponsable(req, res) {
  const payload = await getDataToken(req, res);
  let filtre = {
    responsable: payload.data.idccms,
    status: "closed",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => responsep(1, req, res, result))
    .catch((err) => responsep(2, req, res, err));
}

async function getRequest(req, res) {
  let filtre = {
    areaResponsable: req.body.area,
    status: "closed",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => responsep(1, req, res, result))
    .catch((err) => responsep(2, req, res, err));
}

async function getRequestByIdOpen(req, res) {
  const payload = await getDataToken(req, res);
  let filtre = {
    idccms: payload.data.idccms,
    status: "opend",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => responsep(1, req, res, result))
    .catch((err) => responsep(2, req, res, err));
}

async function getRequestAdminOpen(req, res) {
  let filtre = {
    areaResponsable: req.body.area,
    status: "opend",
  };
  db.request
    .find(filtre)
    .sort({ idrequest: -1 })
    .then((result) => responsep(1, req, res, result))
    .catch((err) => responsep(2, req, res, err));
}

async function getRequestDevOpen(req, res) {
  const payload = await getDataToken(req, res);
  let filter = [
    {
      responsable: payload.data.idccms,
      status: "opend",
    },
    {
      idccms: payload.data.idccms,
      status: "opend",
    },
  ];
  db.request
    .find({ $or: filter })
    .sort({ idrequest: -1 })
    .then((result) => responsep(1, req, res, result))
    .catch((err) => responsep(2, req, res, err));
}

async function downloadBill(req, res) {
  db.request
    .findOne({ "files._id": { $gte: req.query.doc } })
    .then((result) => {
      result.files.forEach((element) => {
        if (element._id == req.query.doc) {
          let file = `${process.cwd()}/${element.path}`;
          res.download(file, element.name);
        }
      });
    })
    .catch((error) => {
      console.log(error);
    });
}

async function downloadBillClosed(req, res) {
  db.request
    .findOne({ "fileClosed.path": { $gte: req.query.doc } })
    .then((result) => {
      let file = `${process.cwd()}/${result.fileClosed.path}`;
      res.download(file, result.fileClosed.name);
    })
    .catch((error) => {
      console.log(error);
    });
}

async function cancelRequest(req, res) {
  let filtre = {
    _id: req.body._id,
  };
  let update = {
    responsable: 0,
    status: "cancel",
  };

  db.request
    .findOneAndUpdate(filtre, [{ $set: update }])
    .then((result) => {
      sockets.getDataGraf(req.body.area);
      sockets.getRequestAdmins(req.body.area);
      responsep(1, req, res, "Cancel successful");
    })
    .catch((err) => responsep(2, req, res, err));
}

async function basicGraft(req, res) {
  let msgGrafic = {
    incoming: 0,
    solved: 0,
  };
  let resp = await db.request
    .find({ status: "closed", areaResponsable: req.body.area })
    .count()
    .then((data) => {
      msgGrafic.solved = data;
    });
  let resp2 = await db.request
    .find({ status: "opend", areaResponsable: req.body.area })
    .count()
    .then((data) => {
      msgGrafic.incoming = data;
    });

  responsep(1, req, res, [
    {
      name: "Pendientes",
      value: msgGrafic.incoming,
    },
    {
      name: "Solucionados",
      value: msgGrafic.solved,
    },
  ]);
}

async function updateEmail(req, res) {
  let filtre = {
    idccms: req.body.idccms,
  };
  let update = {
    email: req.body.email,
  };
  db.user
    .findOneAndUpdate(filtre, [{ $set: update }])
    .then((result) => {
      responsep(1, req, res, "Successful transfer");
    })
    .catch((err) => responsep(2, req, res, err));
}

async function closeRequest(req, res) {
  const payload = await getDataToken(req, res);
  let filtre = {
    _id: req.body._id,
  };
  let tempStatus = JSON.parse(req.body.trackingStatus);
  tempStatus.push({ status: 5, message: "Solved" });
  let update;
  if (req.files.files) {
    update = {
      status: "closed",
      closedPerson: payload.data.nombre,
      closedDatatime: Date.now(),
      trackingStatus: tempStatus,
      closed: req.body.closed,
      fileClosed: {
        name: req.files.files.name,
        path: req.files.files.path.replace("\\", "/"),
        size: req.files.files.size,
        type: req.files.files.type,
      },
    };
  } else {
    update = {
      status: "closed",
      closedPerson: payload.data.nombre,
      trackingStatus: tempStatus,
      closedDatatime: Date.now(),
      closed: req.body.closed,
    };
  }

  db.request
    .findOneAndUpdate(filtre, [{ $set: update }])
    .then((result) => {
      SendEmail(req.body._id, "Tu solicitud fue solucionada", 4, result);
      sockets.getDataGraf(req.body.area);
      sockets.getRequestAdmins(req.body.area);
      responsep(1, req, res, "Updated successful");
    })
    .catch((err) => responsep(2, req, res, err));
}

async function getEmailsAdmin(area) {
  return new Promise((resolve, reject) => {
    let emails = "";
    let filtre = { area: area, rol: "admin" };
    db.user.find(filtre).then((result) => {
      for (let i = 0; i < result.length; i++) {
        if (i == result.length - 1) {
          emails = emails + result[i].email;
        } else {
          emails = emails + result[i].email + ", ";
        }
      }
      resolve(emails);
    });
  });
}

async function SendEmail(emails, subject, type, reqst) {
  let html = "";
  switch (type) {
    case 1:
      html = `<b>Nueva Solicitud</b>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Tipo : </strong> ${reqst.type}</p>
      <p><strong>Descripcion : </strong> ${reqst.problem}</p>
      <p>por favor revisa TP soluccion realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;
      let respL = await getEmailsAdmin(emails).then((res) => {
        emails = res;
      });
      break;
    case 2:
      html = `<b>Transladaron a tu area una solicitud</b>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Tipo : </strong> ${reqst.type}</p>
      <p><strong>Descripcion : </strong> ${reqst.problem}</p>
        <p><strong>Motivo : </strong> ${emails.motive}</p>
      <p>por favor revisa TP soluccion realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;

      let respS = await getEmailsAdmin(emails.emails).then((res) => {
        emails = res;
      });
      break;
    case 3:
      html = `<b>Te asignaron una solicitud</b>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Tipo : </strong> ${reqst.type}</p>
      <p><strong>Descripcion : </strong> ${reqst.problem}</p>
      <p>
        por favor revisa TP soluccion realiza click
        <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a>
      </p>`;
      break;
    case 4:
      html = `<h1>Tu solicitud fue solucionada</h1>
      <p><strong>Numero de referencia: </strong> ${reqst.idrequest}</p>
      <p><strong>Solucion : </strong> ${reqst.closed}</p>
      <p>por favor revisa TP soluccion en la seccion de my historic, realiza click <a href="https://tpsolutionsdemo.teleperformance.co">aquí </a></p>`;
      let filtre = { _id: emails };
      let resp = await db.request
        .findOne(filtre)
        .then((result) => {
          emails = result.email;
        })
        .catch((err) => console.log(err));
      break;
    default:
      break;
  }
  let info = await transporter.sendMail({
    from: '"TP Solution V2" <povedacarvajaltp@gmail.com>',
    to: emails,
    subject: subject,
    html: html,
  });
}

async function ValideAreaPerson(idccms, area) {
  return new Promise((resolve, reject) => {
    let filtre = {
      idccms: idccms,
      area: area,
    };
    db.user
      .findOne(filtre)
      .then((result) => {
        if (result != null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch((err) => {
        console.log(result);
        reject(err);
      });
  });
}

async function transferArea(req, res) {
  let filtre = {
    _id: req.body._id,
  };

  req.body.trackingStatus.push({
    status: 2,
    message: `Transfer to ${req.body.areanew} area`,
  });
  let update = {
    areaResponsable: req.body.areanew,
    areaTransfer: req.body.areaold,
    trackingStatus: req.body.trackingStatus,
    dateTransfer: Date.now(),
    responsable: Number(0),
  };
  db.request
    .findOneAndUpdate(filtre, [{ $set: update }])
    .then((result) => {
      let dataSend = { emails: req.body.areanew, motive: req.body.motive };
      sockets.getRequestIdccms(result.idccms);
      sockets.getRequestAdmins(req.body.areanew);
      sockets.getRequestAdmins(req.body.areaold);
      SendEmail(dataSend, "Transladaron a tu area una solicitud", 2, result);
      responsep(1, req, res, "Successful transfer");
    })
    .catch((err) => responsep(2, req, res, err));
}

async function transferPerson(req, res) {
  const validePerson = await ValideAreaPerson(
    req.body.idccmsResp,
    req.body.area
  );
  if (validePerson) {
    let filtre = {
      _id: req.body._id,
    };
    req.body.trackingStatus.push({
      status: 3,
      message: "Transferred to a person",
    });
    let update = {
      responsable: Number(req.body.idccmsResp),
      trackingStatus: req.body.trackingStatus,
    };
    db.request
      .findOneAndUpdate(filtre, [{ $set: update }])
      .then((result) => {
        sockets.getRequestIdccms(result.idccms);
        sockets.getRequestDevs(req.body.idccmsResp, req.body.area);
        sockets.getRequestAdmins(req.body.area);
        SendEmail(req.body.email, "Te asignaron una solicitud.", 3, result);
        responsep(1, req, res, "Successful transfer");
      })
      .catch((err) => responsep(2, req, res, err));
  } else {
    responsep(2, req, res, "The staff does not belong to your area");
  }
}

async function addTask(req, res) {
  const payload = await getDataToken(req, res);
  console.log(req.body);
  let query = new db.task({
    idccms: payload.data.idccms,
    task: req.body.task,
    description: req.body.description,
    priority: Number(req.body.priority),
    dataTime: req.body.dataTime,
    status: 1,
  });
  query
    .save()
    .then((result) => {
      console.log(result);
      db.task.find({ idccms: payload.data.idccms }).then((datafind) => {
        responsep(1, req, res, datafind);
      });
    })
    .catch((err) => {
      console.log(err);
      responsep(2, req, res, "Creation error");
    });
}

async function finishTask(req, res) {
  let filtre = {
    _id: req.body._id,
  };
  let update = {
    status: 0,
  };
  db.task
    .findOneAndUpdate(filtre, [{ $set: update }])
    .then((result) => {
      console.log(result);
      responsep(1, req, res, "Successful closed task");
    })
    .catch((err) => responsep(2, req, res, err));
}

async function getTask(req, res) {
  const payload = await getDataToken(req, res);
  db.task
    .find({ idccms: payload.data.idccms, status: 1 })
    .then((datafind) => {
      responsep(1, req, res, datafind);
    })
    .catch((err) => {
      console.log(err);
      responsep(2, req, res, "get error");
    });
}

async function getReport(req, res) {
  let excluid = {
    _id: 0,
    idrequest: 0,
    idccms: 0,
    createPerson: 0,
    creationDateTime: 0,
    client: 0,
    email: 0,
    cellphone: 0,
    priority: 0,
    problem: 0,
    dataTimeEstimation: 0,
    trackingStatus: 0,
    files: 0,
    dateTransfer: 0,
    areaTransfer: 0,
    closedDatatime: 0,
    closed: 0,
    finishDate: 0
  };
  console.log(req.body);
  db.request.find({ areaResponsable: req.body.area }, excluid).then((data) => {
    responsep(1, req, res, data);
  });
}

async function getfinishTask(req, res) {
  const payload = await getDataToken(req, res);
  db.task
    .find({ idccms: payload.data.idccms, status: 0 })
    .then((datafind) => {
      responsep(1, req, res, datafind);
    })
    .catch((err) => {
      console.log(err);
      responsep(2, req, res, "get error");
    });
}

let responsep = (tipo, req, res, resultado, cookie) => {
  return new Promise((resolve, reject) => {
    if (tipo == 1) {
      res.cookie("XSRF-TOKEN", req.csrfToken(), {
        "max-Age": new Date(Date.now() + 90000000),
        path: "/",
      });
      res.status(200).json(resultado);
    } else if (tipo == 2) {
      console.log("Error at:", "res: ", resultado);
      res.status(400).json(resultado);
    }
  });
};

module.exports = {
  addRequest,
  getRequestById,
  getRequest,
  addTask,
  getfinishTask,
  finishTask,
  getTask,
  getRequestByIdOpen,
  closeRequest,
  getInfo,
  downloadBill,
  getReport,
  downloadBillClosed,
  getRequestDevOpen,
  getRequestAdminOpen,
  cancelRequest,
  transferArea,
  getRequestByResponsable,
  transferPerson,
  updateEmail,
  addCase,
  basicGraft,
  deleteCase,
  CallSp,
};
