const expressJwt = require('express-jwt');
require("dotenv").config();

exports.jwt = () => {
    const secret  = process.env.SECRET;
    return expressJwt({ secret })
    .unless({
        path: [
            '/api/ccmslogin',
            '/api/refreshToken',
            '/login',
            '/api/download'
        ]
    }); 
}

