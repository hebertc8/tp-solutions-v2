const {
  userJoin,
  userLeave,
  getRoomUsers,
  getIdccms
} = require('../utils/users');
const jwt = require('jsonwebtoken');
const sql = require("./sql.controller");
const routesSql = require("./routesSql.controller")
const parametros = require("./params.controller").parametros;
const config = require("../properties/properties").valor;
const botName = 'Chat Bot';
var arraytem = [];

module.exports = function (server) {
  console.log('Sokect iniciado');
  var io = require('socket.io')(server);
  const chatNsp = io.of('/chat');
  // token validation for sockets

  chatNsp.on('connection', (socket) => {

    console.log('new connection made.');
    console.log(Object.keys(chatNsp.clients().adapter.rooms));

    socket.on('lobby', function (data) {
      let aux = arraytem.findIndex(element => element.username == data);
      if (aux >= 0) {
        arraytem[aux].idsockec = socket.id;
      } else {
        arraytem.push({ username: data, idsockec: socket.id });
      }
    })

    // user login to the room
    socket.on('join', function (data) {
      //joining

      const aux = getIdccms(data.idccms);

      // if (aux) {
      //   chatNsp.in(data.room).emit("disconnect user" + data.idccms, true);
      //   // socket.leave(aux.room);
      //   chatNsp.sockets.connected[aux.id].disconnect();
      //   chatNsp.in(data.room).emit('roomUsers', getRoomUsers(data.room));
      // }
      userJoin(socket.id, data.user, data.room, data.idccms);
      socket.join(data.room);

      console.log(data.user + 'ha ingresado a la sala: ' + data.room);

      // io.to(socket.id).emit('new message private', { user: botName, message: 'Bienvenido a TP Chat', id: '0' });
      // socket.broadcast.to(data.room).emit('new user joined', { user: botName, message: `${data.user} ha ingresado en la sala.` });

    });

    // user disconnection
    socket.on('disconnect', function () {

      const user = userLeave(socket.id);
      if (user[0]) {
        console.log(user[0].username + 'salio de la sala : ' + user[0].room);

        // socket.broadcast.to(user[0].room).emit('left room', { user: botName, message: `${user[0].username} ha salido de la sala.` });

        socket.leave(user[0].room);

        chatNsp.to(user[0].room).emit('roomUsers', getRoomUsers(user[0].room));
      }

    });

    // leave the room
    socket.on('leave', function (data) {

      const user = userLeave(socket.id);
      console.log(data.user + 'left the room : ' + data.room);

      // socket.broadcast.to(data.room).emit('left room', { user: botName, message: `${data.user} ha salido de la sala.` });

      socket.leave(data.room);
      // console.log(data);
      chatNsp.in(data.room).emit('roomUsers', getRoomUsers(data.room));
    });

    // send Message
    socket.on('message', function (data) {

      chatNsp.in(data.room).emit('new message', { user: data.user, message: data.message, id: socket.id });

      aux = arraytem.find(res => res.username == data.name);
      console.log(aux, data)
      if (aux) {
        aux = { username: aux.username, idsockec: aux.idsockec, lobby: data.id }
        chatNsp.to(aux.idsockec).emit('notifi', aux);
      } else {
        sql
          .query('spTPS_GetEmailUser', parametros({ username: data.name }, 'spTPS_GetEmailUser'))
          .then((result) => {
            if (result.Result[0]) {
              console.log(result.Result[0].email);
              routesSql.SendEmail(result.Result[0].email, 'Mensajes sin leer', 5, data.id);
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
      // if (data.rol == '2') {
      //   setTimeout(() => {
      //     io.to(socket.id).emit('new message private', { user: botName, message: 'Gracias por contactarnos, actualmente estamos procesando su solicitud, por favor espere un momento.', id: '0' });
      //   }, 2000);
      // }
    });

    // private Message
    socket.on('private message', function (data) {
      chatNsp.to(data.id).emit('new message private', { user: data.user, message: data.message, id: socket.id });
    })
  });

  /*  AGREGAR ACA SOCKETS TPSOLUTIONS
  
  const grafica = io.of('/grafica');
  grafica.on("connection", (socket) => {
    socket.on("joinRoom", (room) => {
      socket.join(room);
      console.log(`Estas conectado a la room ${room} tu id es ${socket.id}`);
      grafica.in(room).emit("success", "Bienvenido");
    });
    socket.on("error", (data) => {
      console.log(data);
    });
  }); */
  // server.listen(port);
  // server.on('listening', onListening);
}
///////////////////////////////////////////////////////////////////////////////////////
// const db = require("./mongo/models");
// var io;
// let startSocket = (server) => {
//   io = require("socket.io").listen(server);

//   io.of('/graficas').on("connection", (socket) => {
//     socket.on("joinRoom", (room) => {
//       socket.join(room);
//       console.log(`Estas conectado a la room ${room} tu id es ${socket.id}`);
//       io.in(room).emit("success", "Bienvenido");
//     });
//     socket.on("error", (data) => {
//       console.log(data);
//     });
//   });
// };

// async function getRequestIdccms(idccms) {
//   let filtre = {
//     idccms: idccms,
//     status: "opend",
//   };
//   db.request
//     .find(filtre)
//     .sort({ idrequest: -1 })
//     .then((result) => io.in(idccms).emit(`requests`, result));
// }
// async function getRequestDevs(idccms,area) {
//   if (idccms != 0) {
//     let filter = [
//       {
//         responsable: idccms,
//         status: "opend",
//       },
//       {
//         idccms: idccms,
//         status: "opend",
//       },
//     ];
//     db.request
//       .find({ $or: filter })
//       .sort({ idrequest: -1 })
//       .then((result) => io.in(area).emit(`requestsDev`, result));
//   }
// }
// async function getRequestAdmins(area) {
//   let filtre = {
//     areaResponsable: area,
//     status: "opend",
//   };
//   db.request
//     .find(filtre)
//     .sort({ idrequest: -1 })
//     .then((result) => io.in(area).emit(`requestsAdmin`, result));
// }

// async function getDataGraf(area) {
//   let msgGrafic = {
//     incoming: 0,
//     solved: 0,
//   };
//   let resp = await db.request
//     .find({ status: "closed", areaResponsable: area })
//     .count()
//     .then((data) => {
//       msgGrafic.solved = data;
//     });
//   let resp2 = await db.request
//     .find({ status: "opend", areaResponsable: area })
//     .count()
//     .then((data) => {
//       msgGrafic.incoming = data;
//     });

//   io.in(area).emit(`grafico`, [
//     {
//       name: "Pendientes",
//       value: msgGrafic.incoming,
//     },
//     {
//       name: "Solucionados",
//       value: msgGrafic.solved,
//     },
//   ]);
// }

// module.exports = {
//   startSocket,
//   getDataGraf,
//   getRequestIdccms,
//   getRequestDevs,
//   getRequestAdmins,
// };
