const TYPES = require("tedious").TYPES;

let = parametrizacion = (data) => {
  try {
    return data.map(({ name, value, type, schema }) => ({
      nombre: name,
      valor: value,
      tipo: type,
    }));
  } catch (error) {
    console.error(error);
    return error;
  }
};

class SpParam {
  name;
  value;
  type;
  schema;
  constructor(name, value, type, schema = null) {
    this.name = name;
    this.value = value;
    this.type = type;
    this.schema = schema;
  }
}

let = SpParamTable = (nameParam, colums, rows) => {
  try {
    let table;
    let obj = {
      table: [],
    };
    table = {
      columns: colums,
      rows: rows,
    };
    obj.table.push({ nombre: nameParam, valor: table, tipo: TYPES.TVP });
    return obj.table;
  } catch (error) {
    return error;
  }
};

exports.parametros = (req, tipo) => {
  switch (tipo) {
    case "SpTPS_InsertUser":
      return parametrizacion([
        new SpParam("Idccms", Number(req.idccms), TYPES.Int),
        new SpParam("Name", req.name, TYPES.VarChar),
        new SpParam("nUser", req.user, TYPES.VarChar),
      ]);
    case "SpTPS_Login":
      return parametrizacion([new SpParam("Idccms", Number(req), TYPES.Int)]);
    case "SpTPS_Generales":
      return parametrizacion([
        new SpParam("CaseType", req.types, TYPES.Int),
        new SpParam("Area", req.area, TYPES.VarChar),
      ]);
    case "SpTPS_NewRequest":
      return parametrizacion([
        new SpParam("IduserRequest", req.idccms, TYPES.Int),
        new SpParam("Central", req.central, TYPES.Int),
        new SpParam("Descripcion", req.descripcion, TYPES.VarChar),
        new SpParam("DatePromise", req.dataPromise, TYPES.VarChar),
        new SpParam("Incidence", req.incidence, TYPES.Int),
        new SpParam("Area", req.area, TYPES.Int),
        new SpParam("Email", req.email, TYPES.VarChar),
        new SpParam("Telefono", req.telefono, TYPES.VarChar),
      ]);
    case "SpTPS_OpenArea":
      return parametrizacion([new SpParam("Area", req.area, TYPES.VarChar)]);
    case "SpTPS_OpenRequest":
      return parametrizacion([new SpParam("Idccms", req.idccms, TYPES.Int)]);
    case "SpTPS_OpenResponsable":
      return parametrizacion([new SpParam("Idccms", req.idccms, TYPES.Int)]);
    case "SpTPS_ClosedArea":
      return parametrizacion([new SpParam("Area", req.area, TYPES.VarChar)]);
    case "SpTPS_ClosedRequest":
      return parametrizacion([new SpParam("Idccms", req.idccms, TYPES.Int)]);
    case "SpTPS_ClosedResponsable":
      return parametrizacion([new SpParam("Idccms", req.idccms, TYPES.Int)]);
    case "spTPS_SolveRequest":
      return parametrizacion([
        new SpParam("fact_id", req.fact_id, TYPES.Int),
        new SpParam("reporter_id", req.reporter_id, TYPES.Int),
        new SpParam("ClosedDescription", req.ClosedDescription, TYPES.VarChar),
        new SpParam("nameDoc", req.nameDoc, TYPES.VarChar),
        new SpParam("pathDoc", req.pathDoc, TYPES.VarChar),
        new SpParam("sizeDoc", req.sizeDoc, TYPES.Int),
        new SpParam("typeDoc", req.typeDoc, TYPES.VarChar),
      ]);
    case "spTPS_TransferRequest":
      return parametrizacion([
        new SpParam("fact_id", req.fact_id, TYPES.Int),
        new SpParam("IdNewResponsable", req.IdNewResponsable, TYPES.Int),
        new SpParam("Reporter_id", req.Reporter_id, TYPES.Int),
      ]);
    case "spTPS_TransferRequestArea":
      return parametrizacion([
        new SpParam("fact_id", req.fact_id, TYPES.Int),
        new SpParam("newArea", req.newArea, TYPES.Int),
        new SpParam("user_id", req.user_id, TYPES.Int),
      ]);
    case "spTPS_UpdateUserEmail":
      return parametrizacion([
        new SpParam("idccms", req.idccms, TYPES.Int),
        new SpParam("email", req.email, TYPES.VarChar),
      ]);
    case "spTPS_ValidatePersonByArea":
      return parametrizacion([
        new SpParam("idccms", req.idccms, TYPES.Int),
        new SpParam("area", req.area, TYPES.VarChar),
      ]);
    case "spTPS_GetGraphicData":
      return parametrizacion([new SpParam("Area", req.area, TYPES.VarChar)]);
    case "spTPS_GetEmailsUserAdminByArea":
      return parametrizacion([new SpParam("Area", req.area, TYPES.Int)]);
    case "spTPS_GetEmailRequestUser":
      return parametrizacion([new SpParam("fact_id", req.fact_id, TYPES.Int)]);
    case "spTPS_CancelRequest":
      return parametrizacion([
        new SpParam("fact_id", req.fact_id, TYPES.Int),
        new SpParam("user_id", req.user_id, TYPES.Int),
      ]);
    case "spTPS_NewIncidence":
      return parametrizacion([
        new SpParam("name", req.case, TYPES.VarChar),
        new SpParam("area_id", Number(req.area), TYPES.Int),
        new SpParam("project_id", Number(req.project), TYPES.Int),
        new SpParam("priority_id", Number(req.priority), TYPES.Int),
        new SpParam("timeFrame", Number(req.timeType), TYPES.Int),
        new SpParam("legend", req.dataTime, TYPES.VarChar),
        new SpParam("workDays", 1, TYPES.Bit),
        new SpParam("timevalue", Number(req.time), TYPES.Int),
      ]);
    case "spTPS_GetFileListInfo":
      return parametrizacion([new SpParam("id", req.id, TYPES.Int)]);
    case "SpTPS_Report":
      return parametrizacion([new SpParam("Area", req.area, TYPES.VarChar)]);
    case "SpTPS_InsertFiles":
      return parametrizacion([
        new SpParam("IdFact", req.id, TYPES.Int),
        new SpParam("Name", req.name, TYPES.VarChar),
        new SpParam("Path", req.path, TYPES.VarChar),
        new SpParam("Size", req.size, TYPES.VarChar),
        new SpParam("Type", req.type, TYPES.VarChar),
      ]);
    case "spTPS_DeactivateIncidence":
      console.log(req._id);
      return parametrizacion([new SpParam("id", req.id, TYPES.Int)]);
    case "spTPS_InsertarEncuestaSatisfaccion":
      return parametrizacion([
        new SpParam("idFact", req.id, TYPES.Int),
        new SpParam("q1", req.Q1, TYPES.Int),
        new SpParam("q2", req.Q2, TYPES.Int),
        new SpParam("q3", req.Q3, TYPES.VarChar),
      ]);
    //--------------------------------------------------------------------------------------------------
    case "ValideIdccms":
      return parametrizacion([
        { item: "idccms", datos: { valor: req.idccms, tipo: "int" } },
      ]);
    case "spTPChat_GetListConversation":
      return parametrizacion([new SpParam("Room", req.Room, TYPES.VarChar)]);
    case "spGetResponse":
      return parametrizacion([
        new SpParam("Incident", req.Incident, TYPES.VarChar),
      ]);
    case "spTPChat_GetTrackConversation":
      return parametrizacion([
        new SpParam("idConver", req.idConver, TYPES.Int),
      ]);
    case "spTPChat_GetListConversationByUserAdmin":
      return parametrizacion([
        new SpParam("UserAdmin", req.user, TYPES.VarChar),
        new SpParam("Area", req.area, TYPES.VarChar),
      ]);
    case "spTPChat_GetListConversationByUserAgent":
      return parametrizacion([
        new SpParam("UserAgent", req.user, TYPES.VarChar),
      ]);
    case "spTPChatLogin":
      return parametrizacion([
        new SpParam("UserCCMS", req.userccms, TYPES.VarChar),
      ]);
    case "spTPChat_SetConversation":
      return parametrizacion([
        new SpParam("Room", req.Room, TYPES.VarChar),
        new SpParam("UsrAdm", req.UsrAdm, TYPES.VarChar),
        new SpParam("UsrAgt", req.UsrAgt, TYPES.VarChar),
        new SpParam("fact_id", req.FactId, TYPES.Int),
      ]);
    case "spTPChatSetTrackConversation":
      return parametrizacion([
        new SpParam("idConversation", req.idConversation, TYPES.Int),
        new SpParam("User", req.User, TYPES.VarChar),
        new SpParam("Text", req.Text, TYPES.VarChar),
        new SpParam("Read", req.Read, TYPES.Int),
      ]);
    case "spSetAgentInfo":
      return parametrizacion([
        new SpParam("Room", req.Room, TYPES.VarChar),
        new SpParam("UserAgent", req.UserAgent, TYPES.VarChar),
        new SpParam("Direccion", req.Direccion, TYPES.VarChar),
        new SpParam("Ciudad", req.Ciudad, TYPES.VarChar),
      ]);
    case "spTPChat_UpdateConversations":
      return parametrizacion([
        new SpParam("id", req.id, TYPES.Int),
        new SpParam("Room", req.room, TYPES.VarChar),
        new SpParam("UsrAdm", req.usrAdm, TYPES.VarChar),
        new SpParam("Status", req.status, TYPES.Int),
      ]);
    case "spTPChat_GetInfoConversationFact":
      return parametrizacion([new SpParam("fact_id", req.factId, TYPES.Int)]);
    case "spTPS_GetEmailUser":
      return parametrizacion([
        new SpParam("nUser", req.username, TYPES.VarChar),
      ]);
    default:
      break;
  }
};
