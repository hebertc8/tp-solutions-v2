import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';

interface UserInfo {
  idccms: number;
  nombre: string;
  refreshToken: string;
  rol?: string;
  area?: string;
  email?: string;
  token: string;
  username: string;
}

interface UserChat {
  Idccms: number;
  Nombre: string;
  Cargo: string;
  Perfil: string;
  keys: string;
  Direccion: string;
  Ciudad: string;
}

interface rol {
  auth: boolean;
  rol: string;
}

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  error: any;

  constructor(private http: HttpClient) {}

  login(user) {
    const path = environment.apiUrl + '/api/ccmslogin';
    const info = btoa(JSON.stringify(user));
    return this.http.post(path, { body: 's' + info });
  }

  setUser(info) {
    localStorage.setItem(
      'info',
      CryptoJS.AES.encrypt(
        JSON.stringify(info),
        moment(new Date()).format('YYYY-MM-DD')
      ).toString()
    );
  }

  getArea(): string {
    let res = JSON.parse(
      CryptoJS.AES.decrypt(
        localStorage.getItem('info'),
        moment(new Date()).format('YYYY-MM-DD')
      ).toString(CryptoJS.enc.Utf8)
    );

    return res.area;
  }

  getIdccsms(): any {
    let res = JSON.parse(
      CryptoJS.AES.decrypt(
        localStorage.getItem('info'),
        moment(new Date()).format('YYYY-MM-DD')
      ).toString(CryptoJS.enc.Utf8)
    );
    return res.idccms;
  }

  getEmail(): string {
    let res = JSON.parse(
      CryptoJS.AES.decrypt(
        localStorage.getItem('info'),
        moment(new Date()).format('YYYY-MM-DD')
      ).toString(CryptoJS.enc.Utf8)
    );
    return res.email;
  }

  getRol(): rol {
    let res = JSON.parse(
      CryptoJS.AES.decrypt(
        localStorage.getItem('info'),
        moment(new Date()).format('YYYY-MM-DD')
      ).toString(CryptoJS.enc.Utf8)
    );

    let resp;
    if (res.rol == 'admin' || res.rol == 'dev') {
      resp = {
        auth: true,
        rol: res.rol,
      };
    } else {
      resp = {
        auth: false,
        rol: null,
      };
    }

    return resp;
  }

  getUser(): UserInfo {
    try {
      return JSON.parse(
        CryptoJS.AES.decrypt(
          localStorage.getItem('info'),
          moment(new Date()).format('YYYY-MM-DD')
        ).toString(CryptoJS.enc.Utf8)
      );
    } catch (error) {
      this.logout();
    }
  }

  setUserChat(info) {
    localStorage.setItem('chat', CryptoJS.AES.encrypt(JSON.stringify(info), moment(new Date()).format('YYYY-MM-DD')).toString());
  }

  loginChat(user) {
    const path = environment.apiUrl + '/api/login';
    return this.http.post(path, { userccms: user });
  }
  
  getUserChat(): UserChat {
    try {
      return JSON.parse(
        CryptoJS.AES.decrypt(localStorage.getItem('chat'), moment(new Date()).format('YYYY-MM-DD')).toString(CryptoJS.enc.Utf8));
    } catch (error) {
      this.logout();
    }
  }

  logout() {
    localStorage.clear();
  }

  relogin(user) {
    localStorage.removeItem('info');
    localStorage.setItem('info', JSON.stringify(user));
    return true;
  }
}
