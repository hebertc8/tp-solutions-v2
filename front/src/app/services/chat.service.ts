import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { Route } from '@angular/compiler/src/core';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private infoChat;
  private listMsj = [];
  private error;
  private lobby: boolean;

  constructor(private http: HttpClient,
    private login: LoginService,
    private toastr: NbToastrService,
    private router: Router) { }

  private socket = io(environment.apiUrl + '/chat');

  getListRooms() {
    const path = environment.apiUrl + '/api/getListRooms';
    return this.http.post(path, '');
  }

  getListConversation(room) {
    const path = environment.apiUrl + '/api/getListConversation';
    return this.http.post(path, { Room: room });
  }

  getListConversationAdmin(data) {
    const path = environment.apiUrl + '/api/getListConversationAdmin';
    return this.http.post(path, { user: data.username, area: data.area });
  }

  getListConversationAgent(user) {
    const path = environment.apiUrl + '/api/getListConversationAgent';
    return this.http.post(path, { user: user });
  }

  getResponse(incident) {
    const path = environment.apiUrl + '/api/getResponse';
    return this.http.post(path, { Incident: incident });
  }

  getTrackConversation(conver) {
    const path = environment.apiUrl + '/api/getTrackConversation';
    return this.http.post(path, { idConver: conver });
  }

  setConversation(data) {
    const path = environment.apiUrl + '/api/setConversation';
    return this.http.post(path, { Room: data.Room, UsrAdm: data.UsrAdm, UsrAgt: data.UsrAgt, FactId: data.factId});
  }

  updateConversation(data) {
    const path = environment.apiUrl + '/api/updateConversation';
    return this.http.post(path, { id: data.id, room: data.room, usrAdm: data.usrAdm, status: data.status });
  }

  getConversation(data) {
    const path = environment.apiUrl + '/api/getConversationFact';
    return this.http.post(path, { factId: data });
  }

  setTrackConversation(data) {
    const path = environment.apiUrl + '/api/setTrackConversation';
    return this.http.post(path, { idConversation: data.idConversation, User: data.User, Text: data.Text, Read: data.Read });
  }

  // get user information
  getInfoChat() {
    return this.infoChat;
  }

  // set user information
  setInfoChat(data) {
    const id = this.socket.id;
    data = { id: id, user: data.user, room: data.room, idCon: data.idCon };
    this.infoChat = data;
  }

  getlistMsj() {
    return this.listMsj;
  }

  setlistMsj(data) {
    this.listMsj = data;
  }

  getLobby() {
    return this.lobby;
  }

  setLobby(data) {
    this.lobby = data;
  }
  // join the room
  joinRoom(data) {
    this.socket.emit('join', data);
  }

  // new user join
  newUserJoined() {
    let observable = new Observable<{ user: String, message: String }>(observer => {
      this.socket.on('new user joined', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  // leave the room
  leaveRoom(data) {
    this.socket.emit('leave', data);
  }

  // disconnet the room
  disconnetRoom() {
    this.socket.emit('disconnect');
  }

  // the user leaves the room
  userLeftRoom() {
    let observable = new Observable<{ user: String, message: String }>(observer => {
      this.socket.on('left room', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  disconnectUser() {
    let observable = new Observable(observer => {
      this.socket.on('disconnect user' + this.login.getUserChat().Idccms, (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  // Sending messages
  sendMessage(data) {
    this.socket.emit('message', data);
  }

  // new messages received
  newMessageReceived() {
    let observable = new Observable<({ user: String, message: String, id: String })>(observer => {
      this.socket.on('new message', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  // sending private messages
  sendMessagePrivate(data) {
    this.socket.emit('private message', data);
  }

  // new private messages received
  newMessageReceivedPrivate() {
    let observable = new Observable<({ user: String, message: String, id: String })>(observer => {
      this.socket.on('new message private', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  notifiEmit(){
    this.socket.emit('lobby',this.login.getUser().username);
  }

  notificacion() {
    let observable = new Observable<any>(observer => {
      this.socket.on('notifi', (data) => {
        observer.next(data);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }

  // get error
  getError() {
    return this.error;
  }

  // get socket error
  errorSocket() {
    let observable = new Observable<{ error: String }>(observer => {
      this.socket.on('error', (data) => {
        this.error = data;
        console.log(data);
        this.toastr.warning('Session expired', 'Sesión.');
        setTimeout(() => {
          this.login.logout();
          this.router.navigate(['/login']);
          window.location.reload();
        }, 1000);
      });
      return () => { this.socket.disconnect(); }
    });

    return observable;
  }
}
