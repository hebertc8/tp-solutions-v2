import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RequestsService {
  constructor(private http: HttpClient) {}

  get(request) {
    const path = environment.apiUrl + `/api/${request}`;
    return this.http.post(path, {});
  }
  post(request, data) {
    const path = environment.apiUrl + `/api/${request}`;
    return this.http.post(path, data);
  }

  getdonwload(data): Observable<Blob> {
    let Params = new HttpParams();
    Params = Params.append('doc', data.doc);
    const path = environment.apiUrl + `/api/download`;
    return this.http
      .get(path, { params: Params,responseType:'blob' })
      .pipe(map((resObj: Blob) => {

        return resObj
      }));
    /* .map((response: Response)=> response.blob()); */
  }

  getInfo(area) {
    const path = environment.apiUrl + '/api/getInfo';
    return this.http.post(path, {area: area});
  }
  valideIdccms(idccms) {
    const path = environment.apiUrl + '/api/valideIdccms';
    return this.http.post(path, { idccms: idccms });
  }

  refreshSession(refreshToken) {
    const path = environment.apiUrl + '/api/refreshToken';
    return this.http.post(path, { refreshToken });
  }
}
