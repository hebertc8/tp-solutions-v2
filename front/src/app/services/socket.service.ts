import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  resp = new Subject<any>();
  respGrafic = new Subject<any>();
  authRol:boolean
  rol:string;

  socket;
  constructor(private login: LoginService) {
    this.getAuth();
  }

  getAuth() {
    let dataRol = this.login.getRol();
    this.authRol = dataRol.auth;
    this.rol = dataRol.rol;
  }
  startSocket() {
    if (this.socket) {
    } else {
      this.socket = io(environment.apiUrl + '/grafica');
      if (this.authRol) {
        this.socket.emit('joinRoom', this.login.getArea());
      } else {
        this.socket.emit('joinRoom', this.login.getIdccsms());
      }
      this.socket.on('success', (data: any) => {

        this.execSocket();
      });
      this.socket.on('error', (data: any) => {
        console.log(data);
      });
    }
  }

  execSocket() {
    if (!this.authRol) {
      this.socket.on('requests', (data: any) => {
        this.resp.next(data);
      });
    }else if(this.authRol && this.rol == 'dev'){
      this.socket.on('requestsDev', (data: any) => {
        this.resp.next(data);
      });
      this.socket.on('grafico', (data: any) => {
        this.respGrafic.next(data);
      });
    }else{
      this.socket.on('requestsAdmin', (data: any) => {
        this.resp.next(data);
      });
      this.socket.on('grafico', (data: any) => {
        this.respGrafic.next(data);
      });
    }

  }

  getObservable() {
    return this.resp.asObservable();
  }
  getObservableGrafic() {
    return this.respGrafic.asObservable();
  }

  closeSokect() {
    if (this.socket) {
      this.socket.close();
    }
  }
}
