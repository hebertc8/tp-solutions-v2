export class GlobalConstants {
  public static requestMethod = [
    { method: 'Skype' },
    { method: 'Email' },
    { method: 'WhatsApp' },
    { method: 'Teams' },
    { method: 'Call' },
  ];
  public static centrals = [];
  public static areas = [];
  public static areaWorkers = [];
  public static cases = [];
  public static subProject = [];
  /* mapDays: any = new Map([
    [0, 'Sunday'],
    [1, 'Monday'],
    [2, 'Tuesday'],
    [3, 'Wednesday'],
    [4, 'Thursday'],
    [5, 'Friday'],
    [6, 'Saturday'],
  ]); */
}
