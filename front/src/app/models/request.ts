export interface resultData {
  Descripcion?: string;
  Email?: string;
  IdPrioridad?: number;
  IddeIncidencia?: number;
  Legend?: string;
  NombredeIncidencia?: string;
  Status?: string;
  Telefono?: string;
  TimeFrame?: number;
  Timevalue?: number;
  fechadecreacion?: Date;
  fechaestimada?: Date;
  idarearesponsable?: number;
  idccmsdelResponsable?: number;
  idccmsdelCreador?: number;
  idcliente?: number;
  idlasolicitud?: number;
  idproyecto?: number;
  nombrearearesponsable?: string;
  nombredecliente?: string;
  nombredelCreador?: string;
  nombreproyecto?: string;
  LastDateTransfered: Date;
  UltimaArea: string;
  UltimaAreaId: number;
  FechadeCierre: Date;
  DescripcionCierre: string;
  IdUsuerCierre: number;
  TrackingStatus: [{ id: number; message: string }];
  Files: [{ id: number; name: string; class:number; }];
}

export interface dataRequest {
  Result?: resultData[];
  /*  id_?: string;
  idrequest?: number;
  status?: string;
  createPerson?: string;
  creationDateTime?: Date;
  areaResponsable?: string;
  client?: string;
  email?: string;
  cellphone?: string;
  priority?: number;
  type?: string;
  problem?: string;
  dataTimeEstimation?: string;
  files?: [
    {
      name: string;
      path: string;
      size: number;
      type: string;
      _id: string;
    }
  ];
  fileClosed: {
    name: string;
    path: string;
    size: number;
    type: string;
    _id: string;
  };
  areaTransfer?: string;
  dateTransfer?: Date;
  trackingStatus?: [{ status: Number; message: String }];
  closedDatatime?: Date;
  closedPerson?: string;
  closed?: string; */
}
