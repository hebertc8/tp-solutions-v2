export interface infoSelect {
  centrals: [
    {
      id: number,
      name: string;
    }
  ];
  project: [
    {
      id:number;
      name: string;
    }
  ];
  cases: [
    {
      Id: number;
      Nombre?: string,
      Id_Project?: number,
      Project?: string,
      Id_Area?: number,
      Area?: string,
      Priority?: number,
      TimeFrame?:number,
      WorkDays?: boolean,
      Legend?: string,
      TimeValue?: number
    }
  ];
  areaWorkers:[
    {
      idccms: number,
      nameuser: string,
      nUser: string
    }
  ]
  subProject:[
    {
      Id_Project: number,
      Project: string,
      Id_Area: number,
      Area: string
    }
  ]
}
