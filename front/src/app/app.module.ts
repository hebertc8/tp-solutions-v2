import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {
  NbThemeModule,
  NbLayoutModule,
  NbCardModule,
  NbButtonModule,
  NbInputModule,
  NbUserModule,
  NbIconModule,
  NbActionsModule,
  NbSpinnerModule,
  NbContextMenuModule,
  NbMenuModule,
  NbListModule,
  NbCheckboxModule,
  NbToastrModule,
  NbTooltipModule,
  NbRouteTabsetModule,
  NbBadgeModule,
  NbProgressBarModule,
  NbSidebarModule,
  NbSelectModule,
  NbSearchModule,
  NbSidebarService,
  NbMenuService,
  NbDialogModule,
  NbAutocompleteModule,
  NbRadioModule,
  NbChatModule,
  NbDatepickerModule,
  NbTabsetModule,
  NbButtonGroupModule,
} from '@nebular/theme';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpClientXsrfModule,
} from '@angular/common/http';
import { InterceptorService } from './services/interceptor.service';
import { MainComponent } from './container/main/main.component';
import { ContainerComponent } from './container/container.component';
import { FooterComponent } from './container/footer/footer.component';
import { HeaderComponent } from './container/header/header.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DialogUpdateComponent } from './container/main/dialogs/dialog-update/dialog-update.component';
import { ErrorInterceptor } from './services/error-interceptor';
import { ProgressComponent } from './container/progress/progress.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HistoricComponent } from './container/historic/historic.component';
import { BitacoraComponent } from './container/bitacora/bitacora.component';
import { NewRequestedComponent } from './container/requested/new-requested/new-requested.component';
import { DialogConfirmationComponent } from './container/dialog-confirmation/dialog-confirmation.component';
import { SettingsComponent } from './container/admin/settings/settings.component';
import { ValideNumberDirective } from './services/directive/valide-number.directive';
import { ListPriorityComponent } from './container/list-priority/list-priority.component';
import { LobbyComponent } from './container/lobby/lobby.component';
import { ChatComponent } from './container/chat/chat.component';
import { AreaReportingComponent } from './container/area-reporting/area-reporting.component';
import { SurveyComponent } from './container/survey/survey.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    ContainerComponent,
    FooterComponent,
    HeaderComponent,
    DialogUpdateComponent,
    ProgressComponent,
    HistoricComponent,
    BitacoraComponent,
    NewRequestedComponent,
    DialogConfirmationComponent,
    SettingsComponent,
    ValideNumberDirective,
    ListPriorityComponent,
    LobbyComponent,
    ChatComponent,
    AreaReportingComponent,
    SurveyComponent,
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    AppRoutingModule,
    NbButtonGroupModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbCardModule,
    NbTabsetModule,
    NbButtonModule,
    NbDatepickerModule,
    NbDatepickerModule.forRoot(),
    NbInputModule,
    NbAutocompleteModule,
    NbUserModule,
    NbIconModule,
    NbActionsModule,
    NbSpinnerModule,
    NbContextMenuModule,
    NbMenuModule.forRoot(),
    NbListModule,
    NbCheckboxModule,
    NbToastrModule.forRoot(),
    NbTooltipModule,
    NbRouteTabsetModule,
    NbBadgeModule,
    NbProgressBarModule,
    NbSidebarModule,
    NbSelectModule,
    NbSearchModule,
    NbRadioModule,
    NbChatModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'CSRF-Token',
    }),
    Ng2SmartTableModule,
    NbDialogModule.forRoot(),
  ],
  providers: [
    NbSidebarService,
    NbMenuService,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
