import { Component, OnInit, Input, OnDestroy, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';
import { debounce, debounceTime, pluck } from 'rxjs/operators';
import { IfStmt } from '@angular/compiler';
import { LoginService } from 'src/app/services/login.service';
import { obschat } from 'src/app/services/obs-chat';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy, AfterViewInit {

  @Output() status: EventEmitter<String>;

  @ViewChild('divListen') divListen: ElementRef;

  public suscriptor: Subscription;
  title: any;
  user: String;
  currentId: String;
  public room: String;
  rol: String;
  boolRoom: Boolean = false;
  id: String;
  userPrivate: any;
  numberUnreadMsg: number;
  public option;
  public nameChat;
  public idChat;
  public idConver;
  messageArray: Array<{ user: String, message: String, date: Date, type: String, reply: Boolean, read: Boolean }> = [];

  public userSelected;

  constructor(private obschat: obschat, private _chatService: ChatService, private login: LoginService, private toastr: NbToastrService) {
    this.numberUnreadMsg = 0;
    this.status = new EventEmitter;
    const info = this._chatService.getInfoChat();
    this.user = info.user;
    this.room = info.room;
    this.id = info.id;
    this.nameChat = info.idCon.name;
    this.idChat = info.idCon.title;
    this.title = document.title;
    this.userSelected = this.id;
    this.idConver = info.idCon.id;

    _chatService.setLobby(true);

    this._chatService.getTrackConversation(this.idConver).subscribe((res: any) => {
      var rep: Boolean;
      res.forEach(element => {
        if (element.UserId == this.login.getUser().username) {
          rep = true;
        } else {
          rep = false;
        }
        this.messageArray.push({
          user: element.UserId,
          message: element.Text,
          date: new Date(),
          type: 'text',
          reply: rep,
          read: true
        });
      });
    }, err => {
      // console.log('Error', err);
      this.toastr.warning('Internal Error', 'Wrong');
    });

    this._chatService.newUserJoined()
      .subscribe(data => this.messageArray.push({
        user: data.user,
        message: data.message,
        date: new Date(),
        type: 'text',
        reply: false,
        read: true


      }));

    this._chatService.userLeftRoom()
      .subscribe(data => this.messageArray.push({
        user: data.user,
        message: data.message,
        date: new Date(),
        type: 'text',
        reply: false,
        read: true

      }));

    this._chatService.newMessageReceived()
      .subscribe(data => {
        var reply: Boolean;
        if (this.id == data.id) {
          reply = true;
        } else {
          reply = false;
          // this.unReadMessage();
        }
        this.messageArray.push({
          user: data.user,
          message: data.message,
          date: new Date(),
          type: 'text',
          reply: reply,
          read: false
        });

      });
  }

  pushNotifi() {

    if (!Notification) {
      alert('Las notificaciones de escritorio no estan disponibles en tu navegador.');
      return;
    } else if (Notification.permission === "granted") {
      var notification = new Notification("Hola", { icon: './assets/icons/tp/apple-icon.png', body: 'Tienes mas de 10 mensajes sin leer' });
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === "granted") {
          var notification = new Notification("Hola", { icon: './assets/icons/tp/apple-icon.png', body: 'Tienes mas de 10 mensajes sin leer' });
        }
      });
    }
  }

  unReadMessage() {
    const audio = new Audio('assets/notify.wav');
    audio.play();
    this.numberUnreadMsg++;
    var newTitle = '(' + this.numberUnreadMsg + ')' + this.title;
    document.title = newTitle;
    if (this.numberUnreadMsg > 10) {
      this.pushNotifi();
    }
  }

  leave() {
    this.numberUnreadMsg = 0;
    this._chatService.leaveRoom({ user: this.user, room: this.room });
    this.status.emit(this.idChat);
  }

  sendMessage(event: any) {
    this._chatService.sendMessage({ user: this.user, room: this.room, message: event.message, rol: '2', name: this.nameChat, id: this.idChat });
    const username = this.login.getUser().username;
    this._chatService.setTrackConversation({ idConversation: this.idConver, User: username, Text: event.message, Read: 1 }).subscribe((res: any) => {
    }, err => {
      // console.log('Error', err);
      this.toastr.warning('Internal Error', 'Wrong');
    });
  }

  chatRoom() {
    this.boolRoom = false;
    this.currentId = '';
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    const divListen$ = fromEvent(document, 'mouseenter');

    divListen$.pipe(
      // debounceTime(500),
      pluck('x')
    ).subscribe((event) => {
      if (event > 0) {
        if (this.numberUnreadMsg > 0) {

          for (var i = 0; i < this.messageArray.length; i++) {

            if (this.messageArray[i].reply && this.messageArray[i].read == false) {
              this.messageArray[i].read = true;
              this.numberUnreadMsg--;
              if (this.numberUnreadMsg > 0) {
                var newTitle = '(' + this.numberUnreadMsg + ')' + this.title;
                document.title = newTitle;
              } else {
                document.title = this.title;
              }
            } else if (this.messageArray[i].read == false) {
              this.messageArray[i].read = true;
              this.numberUnreadMsg--;
              if (this.numberUnreadMsg > 0) {
                var newTitle = '(' + this.numberUnreadMsg + ')' + this.title;
                document.title = newTitle;
              } else {
                document.title = this.title;
              }
            }
          }
        } else {
          document.title = this.title;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.numberUnreadMsg = 0;
    this._chatService.disconnetRoom();
    let aux = this._chatService.getlistMsj();
    const id = this.idChat
    aux.map(function (d) {
      if (d.lobby == id) {
        d.msj = 0
      }
      return d
    });
    this._chatService.setlistMsj(aux);

    this.status.emit(this.idChat);
  }
}
