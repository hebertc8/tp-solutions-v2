import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { RequestsService } from 'src/app/services/requests.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
})
export class SurveyComponent implements OnInit {
  registerForm: FormGroup;
  constructor(
    private rutaActiva: ActivatedRoute,
    private formBuilder: FormBuilder,
    private requets: RequestsService
  ) {}
  booleanQ1: boolean = true;
  booleanQ2: boolean = true;
  sendSurvey: boolean = false;
  idFact: number;
  ngOnInit(): void {
    this.idFact = Number(this.rutaActiva.snapshot.params.id);
    this.registerForm = this.formBuilder.group({
      Q1: [null],
      Q2: [null],
      Q3: [''],
      id: [this.idFact],
    });
  }

  onSubmit(event: Event) {
    event.preventDefault();
    const input = this.registerForm.value;
    this.requets.post('survey', input).subscribe(
      (res) => {
        this.sendSurvey = true;
      },
      (err) => {
        this.sendSurvey = true;
      }
    );
  }

  setQ1(data) {
    this.registerForm.get('Q1').setValue(data);
    this.booleanQ1 = false;
  }
  setQ2(data) {
    this.registerForm.get('Q2').setValue(data);
    this.booleanQ2 = false;
  }
}
