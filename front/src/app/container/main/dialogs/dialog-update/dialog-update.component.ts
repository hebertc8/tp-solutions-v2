import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { RequestsService } from 'src/app/services/requests.service';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Placeholder {
  central: string;
  mercado: string;
  pais: string;
}
interface UpdateInfo {
  id: number;
  central?: string;
  mercado?: string;
  pais?: string;
}

@Component({
  selector: 'app-dialog-update',
  templateUrl: './dialog-update.component.html',
  styleUrls: ['./dialog-update.component.scss']
})
export class DialogUpdateComponent implements OnInit {
  subscriptionRequets: Subscription;
  public placeholder: Placeholder;
  public updateInfo: UpdateInfo;
  form: FormGroup;
  load = false;
  public requetType = [
    null,
    'update',
    'insert',
    'delete'
  ];

  processNumber = 0;


  constructor(
    protected dialogRef: NbDialogRef<DialogUpdateComponent>,
    private requets: RequestsService, private fb: FormBuilder,
    private toastr: NbToastrService
  ) {
    this.form = fb.group({
      central: [null, Validators.required],
      mercado: [null, Validators.required],
      pais: [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  close(r) {
    this.dialogRef.close(r);
  }


}
