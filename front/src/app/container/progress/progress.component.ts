import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { RequestsService } from 'src/app/services/requests.service';
import { dataRequest, resultData } from '../../models/request';
import { Observable, of, Subscription } from 'rxjs';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { GlobalConstants } from '../../models/global';
import { DialogConfirmationComponent } from '../dialog-confirmation/dialog-confirmation.component';
import { LoginService } from 'src/app/services/login.service';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { SocketService } from 'src/app/services/socket.service';
import * as moment from 'moment';
import { infoSelect } from 'src/app/models/infoSelect';
import { ChatService } from 'src/app/services/chat.service';
import { Router } from '@angular/router';
interface valide {
  nameEmp: string;
}

interface area {
  id: number;
  name: string;
}
interface users {
  idccms: number;
  nameuser: string;
  nUser: string;
}
@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss'],
})
export class ProgressComponent implements OnInit, OnDestroy {
  @ViewChild('autoInput') input;
  public listRequest: resultData[];
  public subcriptionRequest: Subscription;
  public subcriptionSocket: Subscription;
  closed = false;
  transfer = false;
  requestUpdate: number;
  requestProject = '';
  decriptionClosed = '';
  authRol: boolean;
  Rol: string = '';
  time: any;
  public idccms: number = null;
  load = false;
  areaBoolean: boolean = false;
  personBoolean: boolean = false;
  idccmsValide: number = null;
  arrayTracking: number[];
  areas: any[];
  motives: any[];
  tranferId: number;
  areaWorkers: users[] = [
    { nameuser: 'loading...', nUser: '', idccms: 111 },
  ];
  filteredOptions$: Observable<users[]>;
  transferToArea: string = '';
  transferToMotive: string = '';
  files = [];
  hide: boolean = false;
  lenthgStatus: number = null;
  map = new Map<number, string>();

  constructor(
    private Login: LoginService,
    private requets: RequestsService,
    private dialog: NbDialogService,
    private toastr: NbToastrService,
    private socket: SocketService,
    private chat: ChatService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.getAuth();
    this.getData();
    this.getInfoGeneral();
    this.verifyEmail();
    this.time = moment().utcOffset(-300).format();
    if (this.time) {
      setInterval(() => {
        this.time = moment().utcOffset(-300).format();
      }, 60000);
    }
  }



  saveConversation(data) {

    this.chat.getConversation(data.idlasolicitud).subscribe((result: any) => {

      if (result[0]) {
        this.router.navigate(['/container/chat']);
      } else {
        let text = 'Bienvenido al Chat de soporte, Soy el analista encargado de solventar tu solicitud numero: ' + data.idlasolicitud + 'donde nos comentas: ' + data.Descripcion;
        if (data.idccmsdelResponsable) {
          this.chat.setConversation({ Room: data.nombrearearesponsable, UsrAdm: data.idccmsdelResponsable, UsrAgt: this.Login.getUser().username, factId: data.idlasolicitud }).subscribe((res: any) => {

            this.chat.setTrackConversation({ idConversation: res[0].idConversation, User: this.Login.getUser().username, Text: text, Read: 1 }).subscribe((resD: any) => {

              this.router.navigate(['/container/chat']);
            }, (err) => {
              console.log(err);
              this.toastr.warning('Internal Error', 'Wrong');
            });
          }, (err) => {
            console.log(err);
            this.toastr.warning('Internal Error', 'Wrong');
          });
        } else {
          this.chat.setConversation({ Room: data.nombrearearesponsable, UsrAdm: data.nombrearearesponsable, UsrAgt: this.Login.getUser().username, factId: data.idlasolicitud }).subscribe((res: any) => {

            this.chat.setTrackConversation({ idConversation: res[0].idConversation, User: this.Login.getUser().username, Text: text, Read: 1 }).subscribe((resD: any) => {

              this.router.navigate(['/container/chat']);
            }, (err) => {
              console.log(err);
              this.toastr.warning('Internal Error', 'Wrong');
            }
            )
          }, (err) => {
            console.log(err);
            this.toastr.warning('Internal Error', 'Wrong');
          });
        }
      }
    },
      (err) => {
        console.log(err)
        this.toastr.warning('Internal Error', 'Wrong');
      })

  }

  getAuth() {
    let dataRol = this.Login.getRol();
    this.authRol = dataRol.auth;
    this.Rol = dataRol.rol;
    this.idccmsValide = this.Login.getIdccsms();
    this.socket.startSocket();
    this.initSubscriptionSocket();
  }

  initSubscriptionSocket() {
    this.subcriptionSocket = this.socket.getObservable().subscribe((data) => {
      this.listRequest = data;
    });
  }

  refreshData(data:dataRequest) {
    this.listRequest = data.Result;
  }

  cancelRequest(id) {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    let req = {
      _id: id,
    };
    const dialog = this.dialog
      .open(DialogConfirmationComponent, {
        closeOnBackdropClick: false,
        closeOnEsc: false,
        autoFocus: false,
        context: {
          message: 'Are you sure you want to cancel this request? ',
        },
      })
      .onClose.subscribe((resp) => {
        if (resp) {
          this.subcriptionRequest = this.requets
            .post('cancelRequest', req)
            .subscribe(
              (res: any) => {
                this.chat.getConversation(id).subscribe((res: any) => {
                  this.chat.updateConversation({ id: res[0].id, room: res[0].Room, usrAdm: res[0].UserAdmin, status: 2 }).subscribe((res: any) => {
                  }, (err) => {
                    console.log(err);
                  });
                }, (err) => {
                  console.log(err);
                });
                this.toastr.success('Successful cancel', 'Success');
                this.getData();
              },
              (err) => {
                this.toastr.warning('Internal Error', 'Wrong');
              }
            );
        }
      });
  }

  downLoadBill(doc) {
    window.open(environment.apiUrl + `/api/download?doc=${doc}`);
  }
  getData() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    this.load = true;
    switch (this.Rol) {
      case 'admin':
        this.subcriptionRequest = this.requets
          .post('getRequestRolAdmin', { area: this.Login.getArea() })
          .subscribe(
            (res: dataRequest) => {
              this.load = false;
              this.listRequest = res.Result;
            },
            (err) => {
              this.load = false;
            }
          );
        break;
      case 'dev':
        this.subcriptionRequest = this.requets.post('getRequestRol', { idccms: this.Login.getIdccsms() }).subscribe(
          (res: dataRequest) => {
            this.load = false;
            this.listRequest = res.Result;
          },
          (err) => {
            this.load = false;
          }
        );
        break;
      default:
        this.subcriptionRequest = this.requets
          .post('getRequestByIdOpen', { idccms: this.Login.getIdccsms() })
          .subscribe(
            (res: dataRequest) => {
              this.load = false;
              this.listRequest = res.Result;
            },
            (err) => {
              this.load = false;
            }
          );
        break;
    }
  }

  backIcon() {
    this.load = false;
    this.areaBoolean = false;
    this.transfer = false;
    this.personBoolean = false;
    this.transferToArea = '';
  }

  saveClosed(id) {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    const formData = new FormData();
    if (this.files.length >= 1) {
      formData.append('files', this.files[0], this.files[0].name);
    }
    formData.append('closed', this.decriptionClosed);
    formData.append('_id', id);
    formData.append('trackingStatus', JSON.stringify(this.arrayTracking));
    formData.append('area', this.Login.getArea());
    this.load = true;

    this.subcriptionRequest = this.requets
      .post('closeRequest', formData)
      .subscribe(
        (res: dataRequest) => {
          this.load = false;
          this.decriptionClosed = '';
          this.toastr.success('Closed requets', 'Success');
          this.getData();
          this.closed = !this.closed;
        },
        (err) => {
          this.load = false;
        }
      );
  }

  getInfoGeneral() {
    let dataArea = this.Login.getArea();
    this.requets.getInfo(dataArea).subscribe(
      (res: infoSelect) => {
        let AreaPerson = res.areaWorkers;
        AreaPerson.forEach((el) => {
          this.map.set(el.idccms, el.nameuser);
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }
  updateTransfer(id, idccms, type) {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    let msgConf;



    if (type == 1) {
      msgConf = `I want to assign it to ${idccms}.`;
    } else {
      msgConf = `I want to assign it to  ${this.transferToArea}`;
    }
    const dialog = this.dialog
      .open(DialogConfirmationComponent, {
        closeOnBackdropClick: false,
        closeOnEsc: false,
        autoFocus: false,
        context: {
          message: msgConf,
        },
      })
      .onClose.subscribe((resp) => {
        if (resp) {
          this.load = true;
          if (type == 1) {
            this.subcriptionRequest = this.requets
              .post('transferPerson', {
                _id: id.idlasolicitud,
                idccmsResp: idccms,
                problem: id.Descripcion,
                type: id.NombredeIncidencia,
                area: this.Login.getArea(),
                email: this.Login.getEmail(),
              })
              .subscribe(
                (res) => {
                  let filter = this.areaWorkers.find(d => d.idccms == idccms)
                  this.load = false;
                  this.getData();
                  this.toastr.success('Successful transfer', 'Success');
                  this.transfer = !this.transfer;
                  this.personBoolean = !this.personBoolean;

                  this.chat.getConversation(id.idlasolicitud).subscribe((res: any) => {

                    var aux
                    if (res[0].StatusIncident == "Close") {
                      aux = 2
                    } else {
                      aux = 1
                    }
                    this.chat.updateConversation({ id: res[0].id, room: res[0].Room, usrAdm: filter.nUser, status: aux }).subscribe((res: any) => {

                    }, (err) => {
                      console.log(err);
                    });
                  }, (err) => {
                    console.log(err);
                  });
                },
                (err) => {
                  console.log(err);
                  this.toastr.danger(
                    'The staff does not belong to your area',
                    'Failure'
                  );
                  this.load = false;
                }
              );
          } else {
            let newArea = this.areas.find(
              (d) => d.name == this.transferToArea
            );
            this.subcriptionRequest = this.requets
              .post('transferArea', {
                _id: id.idlasolicitud,
                problem: id.Descripcion,
                type: id.NombredeIncidencia,
                newArea: newArea.id,
                areaold: this.Login.getArea(),
                motive: this.transferToMotive,
              })
              .subscribe(
                (res) => {
                  this.load = false;
                  this.getData();
                  this.toastr.success('Successful transfer', 'Success');
                  this.transfer = !this.transfer;
                  this.areaBoolean = !this.areaBoolean;
                  this.chat.getConversation(id.idlasolicitud).subscribe((res: any) => {
                    var aux
                    if (res[0].StatusIncident == "Close") {
                      aux = 2
                    } else {
                      aux = 1
                    }
                    this.chat.updateConversation({ id: res[0].id, room: newArea.name, usrAdm: res[0].UserAdmin, status: aux }).subscribe((res: any) => {

                    }, (err) => {
                      console.log(err);
                    });
                  }, (err) => {
                    console.log(err);
                  });
                },
                (err) => {
                  this.load = false;
                }
              );
          }
        } else {
          this.load = false;
          this.areaBoolean = false;
          this.transfer = false;
          this.personBoolean = false;

          //volver todo al inicio.
        }
      });
  }
  verifyEmail() {
    const email = this.Login.getEmail();
    if (email == 'NULL') {
      setTimeout(() => {
        let msgConf = `verifyEmail`;
        this.dialog.open(DialogConfirmationComponent, {
          closeOnBackdropClick: true,
          closeOnEsc: true,
          autoFocus: true,
          context: {
            message: msgConf,
          },
        });
      }, 1000);
    }
  }
  transferArea() {
    this.areaBoolean = true;
    this.areas = GlobalConstants.areas;
    this.fetchMotives();
  }
  fetchMotives() {
    this.motives = [
      {
        name: 'No pertenece al Area',
      },
      { name: 'Sin personal' },
    ];
  }
  getArea(event) {
    this.transferToArea = event;
  }
  getMotives(event) {
    this.transferToMotive = event;
  }
  transferPerson() {
    this.personBoolean = true;

    this.areaWorkers = GlobalConstants.areaWorkers;
    this.filteredOptions$ = of(this.areaWorkers);
  }

  private filter(value: string): users[] {
    this.tranferId = Number(value);
    const filterValue = value;
    return this.areaWorkers.filter((optionValue) =>
      optionValue.nameuser.toLowerCase().includes(filterValue)
    );
  }

  onChange() {
    this.filteredOptions$ = this.getFilteredOptions(
      this.input.nativeElement.value
    );
  }
  getFilteredOptions(value: string): Observable<users[]> {
    return of(value).pipe(map((filterString) => this.filter(filterString)));
  }
  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }
  fileLoad(element) {
    //#region capturar documento
    let uploadedfiles = element.target.files;
    this.files.push(uploadedfiles[0]);
    this.hide = true;
    //#endregion
  }

  removeFile(size, name, lastModified) {
    this.files = this.files.filter(
      (e) =>
        e.name !== name && e.size !== size && e.lastModified !== lastModified
    );
    this.hide = false;
  }
  transferOpen(idrequest) {
    this.requestUpdate = idrequest;
    this.transfer = !this.transfer;
  }
  cancelClosed() {
    this.closed = !this.closed;
    this.decriptionClosed = '';
    this.files = [];
    this.hide = false;
  }

  closedRequest(id, project) {
    this.closed = !this.closed;
    this.requestUpdate = id;
    this.requestProject = project;
    this.chat.getConversation(id).subscribe((res: any) => {
      this.chat.updateConversation({ id: res[0].id, room: res[0].Room, usrAdm: res[0].UserAdmin, status: 2 }).subscribe((res: any) => {

      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err);
    });
  }
  ngOnDestroy() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
  }
}
