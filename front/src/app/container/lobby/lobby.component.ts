import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { LoginService } from 'src/app/services/login.service';
import { NbToastrService } from '@nebular/theme';
import { obschat } from 'src/app/services/obs-chat';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {

  users: { name: any, id: any, title: any, msj: number }[] = [];


  cambioRoom: String;
  room: String;
  public option;
  public direction;
  public city;
  public rol;
  public roomArray: Array<any> = [];
  public spinnerLog: Boolean = false;
  public spinnerList: Boolean = true;

  constructor(private obschat: obschat,
    private _chatService: ChatService,
    private login: LoginService,
    private toastr: NbToastrService) {

    _chatService.notificacion().subscribe((res: any) => {
      if (res == 1) {

      } else {
        let aux = this.users.findIndex(data => data.title == res.lobby);
        if (aux >= 0) {
          this.users[aux].msj = this.users[aux].msj + 1
        } else {

        }
      }
    });

    this.roomArray = _chatService.getlistMsj();
    document.title = 'TP Solution';
    this._chatService.errorSocket()
      .subscribe(data => {
        console.log('ERROR SOCKET' + data);
      });
    this.rol = this.login.getRol().auth;
    // this.rol = 'view';

    if (!this.rol) {
      this._chatService.getListConversationAgent(this.login.getUser().username).subscribe((res: any) => {
        if (res.length) {
          res.forEach(element => {
            let aux = this.roomArray.findIndex(data => data.lobby == element.fact_id);
            if (aux >= 0) {
              this.users.push({ name: element.UserAdmin, id: element.id, title: element.fact_id, msj: this.roomArray[aux].msj });
            } else {
              this.users.push({ name: element.UserAdmin, id: element.id, title: element.fact_id, msj: 0 });
            }
          });
        } else {
          this.users.push({ name: 'No hay conversaciones', id: '', title: '', msj: 0 })
        }
        this.spinnerList = false;
      }, err => {
        this.toastr.warning('Internal Error', 'Wrong');
      });
    } else {
      this._chatService.getListConversationAdmin({ username: this.login.getUser().username, area: this.login.getArea() }).subscribe((res: any) => {
        
        if (res.length) {
          res.forEach(element => {
            let aux = this.roomArray.findIndex(data => data.lobby == element.fact_id);
            if (aux >= 0) {
              this.users.push({ name: element.UserAgent, id: element.id, title: element.fact_id, msj: this.roomArray[aux].msj });
            } else {
              this.users.push({ name: element.UserAgent, id: element.id, title: element.fact_id, msj: 0 });
            }
          });
        } else {
          this.users.push({ name: 'No hay conversaciones', id: '', title: '', msj: 0 })
        }
        this.spinnerList = false;
      }, err => {
        // console.log('Error', err);

        this.toastr.warning('Internal Error', 'Wrong');
      });
    }


  }

  join(user) {
    var error = this._chatService.getError();
    this.spinnerLog = true;
    if (error) {
      this.spinnerLog = false;
      this.toastr.warning(error, 'Error');
    } else {
      this.changeComponent(user);
    }

  }

  changeComponent(user) {
    let aux = this.users.findIndex(ele => ele.title == user.title);
    this.users[aux].msj = 0;
    this.roomArray.map(function (d) {
      if (d.lobby == user.title) {
        d.msj = 0
      }
      return d
    });
    this._chatService.setlistMsj(this.roomArray);
    if (!this.rol) {
      var name = this.login.getUser().nombre.split(' ');
      var username = name[0] + ' ' + name[1];
      this.room = user.id + this.login.getUser().username;
      this._chatService.joinRoom({ user: username, room: this.room, idccms: this.login.getUser().idccms });
      this._chatService.setInfoChat({ user: username, room: this.room, idCon: user });
      this.cambioRoom = 'Si';
    } else {
      var name = this.login.getUser().nombre.split(' ');
      var username = name[0] + ' ' + name[1];
      this.room = user.id + user.name;
      this._chatService.joinRoom({ user: username, room: this.room, idccms: this.login.getUser().idccms });
      this._chatService.setInfoChat({ user: username, room: this.room, idCon: user });
      this.cambioRoom = 'Si';
    }
    // this.obschat.datos$.emit(true);

  }

  change(aux: String) {
    this.spinnerLog = false;
    this.users.map(function (d) {
      if (d.title == aux) {
        d.msj = 0
      }
      return d
    });
    this.roomArray.map(function (d) {
      if (d.lobby == aux) {
        d.msj = 0
      }
      return d
    });
    this._chatService.setlistMsj(this.roomArray);

    this.cambioRoom = 'No';
  }

  ngOnInit(): void {
    this.obschat.datos$.emit(true);
    this._chatService.setLobby(true);
  }

  ngOnDestroy(): void {
    this._chatService.setLobby(false);
  }

}
