import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from '../../../models/global';
import { RequestsService } from 'src/app/services/requests.service';
import { Subscription, Observable, of } from 'rxjs';
import { dataRequest } from '../../../models/request';
import { map } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';
import { LoginService } from 'src/app/services/login.service';
import { ValideNumberDirective } from '../../../services/directive/valide-number.directive';
import * as moment from 'moment';
import { ChatService } from 'src/app/services/chat.service';
interface centrals {
  id: number;
  name: string;
}
@Component({
  selector: 'app-new-requested',
  templateUrl: './new-requested.component.html',
  styleUrls: ['./new-requested.component.scss'],
})
export class NewRequestedComponent implements OnInit, OnDestroy {
  @Output()
  EmmiterList = new EventEmitter<dataRequest>();

  @ViewChild('autoInput') input;
  registerForm: FormGroup;
  centrals = [{ id: null, name: 'Loading ...' }];
  area = [{ id: null, name: 'Loading ...' }];
  method = GlobalConstants.requestMethod;
  clienteValue = '';
  global: any;
  case = [
    {
      Id_Area: null,
      Id_Project: null,
      Id: null,
      Area: '',
      Legend: '',
      Nombre: 'Loading ...',
      Priority: null,
      Project: '',
      Schedule: null,
      TimeFrame: null,
      TimeValue: null,
      WorkDays: null,
    },
  ];
  caseTemp = [
    {
      Id_Area: null,
      Id_Project: null,
      Id: null,
      Area: '',
      Legend: '',
      Nombre: 'Loading ...',
      Priority: null,
      Project: '',
      Schedule: null,
      TimeFrame: null,
      TimeValue: null,
      WorkDays: null,
    },
  ];
  files = [];
  subProject = [
    {
      Id_Project: null,
      Project: 'loading ...',
      Id_Area: null,
      Area: '',
    },
  ];
  subProjectTemp = [
    {
      Id_Project: null,
      Project: 'loading ...',
      Id_Area: null,
      Area: '',
    },
  ];
  options: string[];
  filteredOptions$: Observable<centrals[]>;
  priority: number = null;
  dataTimeEstimation: string = '';
  areaSelect: string;
  booleanProject: boolean = false;
  typeSelect: boolean = false;
  endDataTime: Date;

  public subcriptionRequest: Subscription;
  load = false;
  open = true;
  authRol: boolean;
  Rol: string = '';
  constructor(
    private formBuilder: FormBuilder,
    private requets: RequestsService,
    private toastr: NbToastrService,
    private Login: LoginService,
    private chat: ChatService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      areaResponsable: ['', Validators.required],
      responsable: [null],
      client: [''],
      email: ['', [Validators.email, Validators.required]],
      cellphone: ['', Validators.required],
      type: ['', Validators.required],
      problem: ['', Validators.required],
      priority: [''],
      dataTimeEstimation: [''],
      finishDate: [''],
      project: [''],
    });
    this.getAuth();
  }
  getAuth() {
    let dataRol = this.Login.getRol();
    this.authRol = dataRol.auth;
    this.Rol = dataRol.rol;
    /* alert(this.authRol + ' - ' + this.Rol); */
  }

  saveFilterProject(project) {
    this.case = this.caseTemp.filter(
      (d) =>
        d.Id_Area == this.areaSelect &&
        (d.Project == 'Global' || d.Id_Project == project)
    );
    this.typeSelect = true;
    this.registerForm.controls['type'].reset();
  }
  saveResponsable(area) {
    this.subProject = this.subProjectTemp.filter((d) => d.Id_Area == area);
    let globalData = this.subProject.find((d) => d.Project == 'Global');
    this.global = globalData.Id_Project;
    this.subProject = this.subProject.filter((d) => d.Project != 'Global');
    if (this.subProject.length > 0) {
      this.booleanProject = true;
      this.areaSelect = area;
      this.typeSelect = false;
    } else {
      console.log('entro');

      this.booleanProject = false;
      this.typeSelect = true;
      this.registerForm.controls['project'].setValue(this.global);
      this.case = this.caseTemp.filter((d) => d.Id_Area == area);
    }
    this.registerForm.controls['type'].reset();
    this.registerForm.controls['project'].reset();
  }
  saveCase(cases) {
    let data = this.case.find((d) => d.Id == cases);
    this.priority = data.Priority;
    this.dataTimeEstimation = data.Legend;
    this.valideEnableTime(data);
  }

  valideEnableTime(dataCases) {
    let finishTime;
    switch (dataCases.TimeFrame) {
      case 1:
        finishTime = moment()
          .utcOffset(-300)
          .add(dataCases.TimeValue, 'minutes')
          .format('YYYYMMDD HH:mm:ss');
        this.endDataTime = finishTime;
        break;
      case 2:
        finishTime = moment()
          .utcOffset(-300)
          .add(dataCases.TimeValue, 'hours')
          .format('YYYYMMDD HH:mm:ss');
        this.endDataTime = finishTime;
        break;
      case 3:
        this.filterWorkDays(dataCases.TimeValue);
        break;
      default:
        break;
    }
  }
  filterWorkDays(time) {
    let days = time;
    let finishTime;
    for (let index = 1; index <= days; index++) {
      let tempDay = moment().add(index, 'days').days();
      if (tempDay === 0 || tempDay === 6) {
        days += 1;
      }
    }
    finishTime = moment()
      .utcOffset(-300)
      .add(days, 'days')
      .format('YYYYMMDD HH:mm:ss');
    this.endDataTime = finishTime;
  }

  removeFile(size, name, lastModified) {
    this.files = this.files.filter(
      (e) =>
        e.name !== name && e.size !== size && e.lastModified !== lastModified
    );
  }

  getPriority() {
    let idcentral = this.centrals.find((d) => d.name == this.clienteValue);
    this.registerForm.controls['client'].setValue(idcentral.id);
    this.registerForm.controls['priority'].setValue(this.priority);
    this.registerForm.controls['dataTimeEstimation'].setValue(
      this.dataTimeEstimation
    );
  }
  private filter(value: string): centrals[] {
    const filterValue = value.toLowerCase();
    this.clienteValue = value;
    return this.centrals.filter((optionValue) =>
      optionValue.name.toLowerCase().includes(filterValue)
    );
  }

  getFilteredOptions(value: string): Observable<centrals[]> {
    return of(value).pipe(map((filterString) => this.filter(filterString)));
  }

  onChange() {
    this.filteredOptions$ = this.getFilteredOptions(
      this.input.nativeElement.value
    );
  }

  onSelectionChange($event) {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }

  fileLoad(element) {
    //#region capturar documento
    let uploadedfiles = element.target.files;
    if (this.files.length) {
      let cond = this.files.findIndex(
        (data) => data.name == uploadedfiles[0].name
      );

      if (cond < 0) {
        this.files.push(uploadedfiles[0]);
      }
    } else {
      this.files.push(uploadedfiles[0]);
    }
    //#endregion
  }

  newRequest() {
    this.centrals = GlobalConstants.centrals;
    this.area = GlobalConstants.areas;
    this.case = GlobalConstants.cases;
    this.caseTemp = GlobalConstants.cases;
    this.subProject = GlobalConstants.subProject;
    this.subProjectTemp = GlobalConstants.subProject;
    this.filteredOptions$ = of(this.centrals);
    this.open = !this.open;
    this.typeSelect = false;
    this.registerForm.reset();
    this.files = [];
  }
  valideClient() {
    let res = this.centrals.find(
      (data) => data.name.toUpperCase() == this.clienteValue.toUpperCase()
    );
    if (res) {
      return true;
    } else {
      return false;
    }
  }

  onSubmit() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    if (this.valideClient()) {
      this.load = true;
      this.getPriority();
      const formData = new FormData();

      for (let i = 0; i < this.files.length; i++) {
        formData.append('files', this.files[i], this.files[i].name);
      }
      this.registerForm.get('responsable').setValue(0);
      this.registerForm.get('finishDate').setValue(this.endDataTime);
      Object.entries(this.registerForm.value).forEach(([key, value]) => {
        formData.append(key, this.registerForm.get(key).value);
      });
      let newArea = this.area.find(
        (d) => d.id == this.registerForm.get('areaResponsable').value
      );
      this.subcriptionRequest = this.requets
        .post('addRequest', formData)
        .subscribe(
          (res) => {
            this.newRequest();
            this.toastr.success('OK', 'Success');
            this.getData();
            this.registerForm.reset();
            this.load = false;
          },
          (err) => {
            this.load = false;
            console.log(err);
            this.toastr.warning(err, 'Wrong Log');
          }
        );
    } else {
      this.toastr.warning('invalid client', 'Wrong');
    }
  }
  getData() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    switch (this.Rol) {
      case 'admin':
        this.subcriptionRequest = this.requets
          .post('getRequestRolAdmin', { area: this.Login.getArea() })
          .subscribe(
            (res: dataRequest) => {
              this.EmmiterList.emit(res);
            },
            (err) => {}
          );
        break;
      case 'dev':
        this.subcriptionRequest = this.requets
          .post('getRequestRol', { idccms: this.Login.getIdccsms() })
          .subscribe(
            (res: dataRequest) => {
              this.EmmiterList.emit(res);
            },
            (err) => {}
          );
        break;
      default:
        this.subcriptionRequest = this.requets
          .post('getRequestByIdOpen', { idccms: this.Login.getIdccsms() })
          .subscribe(
            (res: dataRequest) => {
              this.EmmiterList.emit(res);
            },
            (err) => {}
          );
        break;
    }
  }

  ngOnDestroy() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
  }
}
