import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import * as moment from 'moment';
import { GlobalConstants } from 'src/app/models/global';
import { infoSelect } from 'src/app/models/infoSelect';
import { LoginService } from 'src/app/services/login.service';
import { RequestsService } from 'src/app/services/requests.service';
import { task } from '../list-priority/list-priority.component';

interface arrayReport{
  Result: report[];
}
interface report {
  IdStatus?: number;
  idIncidencia?: number;
  idarearesponsable?: number;
  idproyecto?: number;
  idresponsable?: number;
  IdUsuerCierre?:number;
  closedPerson?: string;
}

interface reportInfo {
  name: string;
  asig: number;
  pending: number;
  closed: number;
}

interface persondata {
  name: string;
  idccms: number;
  email: string;
}

interface subProject {
  Id_Project: number;
  Project: string;
  Id_Area: number;
  Area: string;
}

interface cases {
  Id: number;
  Nombre?: string;
  Id_Project?: number;
  Project?: string;
  Id_Area?: number;
  Area?: string;
  Priority?: number;
  TimeFrame?: number;
  WorkDays?: boolean;
  Legend?: string;
  TimeValue?: number;
}

@Component({
  selector: 'app-area-reporting',
  templateUrl: './area-reporting.component.html',
  styleUrls: ['./area-reporting.component.scss'],
})
export class AreaReportingComponent implements OnInit {
  settings = {
    mode: 'external',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    attr: {
      class: 'tablePrimary',
    },
    columns: {
      name: {
        title: 'Name',
        filter: false,
        editable: false,
      },
      asig: {
        title: 'Asignados',
        filter: false,
        editable: false,
      },
      pending: {
        title: 'pendientes',
        filter: false,
        editable: false,
      },
      closed: {
        title: 'cerrados',
        filter: false,
        editable: false,
      },
    },
  };
  source: reportInfo[] = [];
  sourceany: any[];
  listPerson: any[];
  subProject: subProject[] = [
    {
      Id_Project: null,
      Project: 'Loading',
      Id_Area: null,
      Area: '',
    },
  ];
  subProjectTemp: subProject[] = [
    {
      Id_Project: null,
      Project: 'Loading',
      Id_Area: null,
      Area: '',
    },
  ];
  cases: cases[] = [
    {
      Id: null,
      Nombre: 'loading ...',
      Id_Project: null,
      Project: '',
      Id_Area: null,
      Area: '',
      Priority: null,
      TimeFrame: null,
      WorkDays: null,
      Legend: '',
      TimeValue: null,
    },
  ];
  casesTemp: cases[] = [
    {
      Id: null,
      Nombre: 'loading ...',
      Id_Project: null,
      Project: '',
      Id_Area: null,
      Area: '',
      Priority: null,
      TimeFrame: null,
      WorkDays: null,
      Legend: '',
      TimeValue: null,
    },
  ];
  booleanspring: boolean = false;
  typeSelect: boolean = false;
  map = new Map<number, string>();
  inversMap = new Map<string, number>();
  dataBasic: report[] = [];
  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
    private dialog: NbDialogService,
    private login: LoginService,
    private request: RequestsService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      project: [''],
      case: [''],
    });
    this.getInfoGeneralGlobal();
  }

  getTask() {
    this.request.post('getReport', { area: this.login.getArea() }).subscribe(
      (res: arrayReport) => {
        this.dataBasic = res.Result.filter((d) => d.IdStatus != 3);
        this.process(this.dataBasic);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getInfoGeneral() {
    let listPerson = GlobalConstants.areaWorkers;
    listPerson.forEach((el) => {
      this.map.set(el.idccms, el.nameuser);
      this.inversMap.set(el.nameuser, el.idccms);
    });
    this.getTask();
  }

  getInfoGeneralGlobal() {
    this.booleanspring = true;
    let dataArea = this.login.getArea();
    this.request.getInfo(dataArea).subscribe(
      (res: infoSelect) => {
        GlobalConstants.areaWorkers = res.areaWorkers;
        this.cases = res.cases.filter((d) => d.Area == this.login.getArea());
        this.casesTemp = this.cases;
        this.subProject = res.subProject.filter(
          (d) => d.Area == this.login.getArea()
        );
        this.subProject = this.subProject.filter(
          (d) => d.Project != 'Global'
        );
        this.subProjectTemp = this.subProject;
        this.getInfoGeneral();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  projectDropDown(event) {
    this.booleanspring = true;
    this.registerForm.controls['case'].reset();
    this.cases = this.casesTemp.filter((d) => (d.Project == 'Global' || d.Id_Project == event));
    this.typeSelect = true;
    let temp = this.dataBasic.filter((d) => d.idproyecto == event);
    this.source = [];

    this.process(temp);
  }

  resetFilter() {
    this.registerForm.controls['project'].reset();
    this.typeSelect = false;
    this.registerForm.controls['case'].reset();
    this.source = [];
    this.process(this.dataBasic);
  }

  caseDropDown(event) {
    let temp = this.dataBasic.filter((d) => d.idIncidencia == event);
    this.source = [];
    this.process(temp);
  }

  process(res: report[]) {
    let listPerson: persondata[] = GlobalConstants.areaWorkers;
    let name: string;
    let asig: number;
    let pending: number;
    let closed: number;
    // sin asignar
    let AsignFilter = res.filter((d) => d.idresponsable == 0);
    let asignclosed = AsignFilter.filter((d) => d.IdStatus == 2);
    if (AsignFilter.length != 0) {
      this.source.push({
        name: 'Sin asignar',
        asig: AsignFilter.length,
        pending: AsignFilter.length - asignclosed.length,
        closed: asignclosed.length,
      });
    }
    // asignados
    listPerson.forEach((el) => {
      name = '';
      let basicFilter = res.filter((d) => d.idresponsable == el.idccms);
      if (basicFilter.length != 0) {
        name = this.map.get(el.idccms);
        asig = basicFilter.length;
      } else {
        asig = 0;
      }
      let pendingFilter = basicFilter.filter((d) => d.IdStatus == 1);
      if (basicFilter.length != 0) {
        name = this.map.get(el.idccms);
        pending = pendingFilter.length;
      } else {
        pending = 0;
      }
      let closedFilter = res.filter((d) => d.IdUsuerCierre == el.idccms);
      let validclosed = closedFilter.filter((d) => d.IdStatus == 2);
      if (validclosed.length != 0) {
        name = this.map.get(el.idccms);
        closed = validclosed.length;
      } else {
        pending = 0;
      }

      if (name != '') {
        this.source.push({
          name: name,
          asig: asig,
          pending: pending,
          closed: closed,
        });
        name = '';
      }
    });
    this.sourceany = this.source;
    this.booleanspring = false;
  }
}
