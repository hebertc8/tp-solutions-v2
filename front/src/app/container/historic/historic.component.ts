import { Component, OnInit, OnDestroy } from '@angular/core';
import { dataRequest, resultData } from 'src/app/models/request';
import { RequestsService } from 'src/app/services/requests.service';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/services/login.service';
import { saveAs } from 'file-saver';
import { SocketService } from 'src/app/services/socket.service';
import { environment } from 'src/environments/environment';
import { infoSelect } from 'src/app/models/infoSelect';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.scss'],
})
export class HistoricComponent implements OnInit, OnDestroy {
  public test: resultData[];
  public testStatic: resultData[];
  public subcriptionRequest: Subscription;
  public subcriptionSocket: Subscription;
  closed = false;
  requestUpdate = 0;
  decriptionClosed = '';
  idreqst: number = null;
  load = false;
  authRol: boolean;
  Rol: string = '';
  structureSingle: any[] = [];
  view: any[] = [400, 200];
  single: any[] = [];
  colorScheme = {
    domain: ['#A10A28', '#5AA454'],
  };
  map = new Map<number, string>();

  constructor(
    private Login: LoginService,
    private requets: RequestsService,
    private socket: SocketService
  ) {
    this.initSubscriptionSocket();
    this.socket.startSocket();
  }

  ngOnInit(): void {
    this.getAuth();
    this.getData();
    this.dataGraftInit();
    this.getInfoGeneral();
  }

  initSubscriptionSocket() {
    this.subcriptionSocket = this.socket
      .getObservableGrafic()
      .subscribe((data) => {
        let pendientes;
        let solucionados;
        data.name.forEach((element) => {
          if (element.Solucionados) {
            solucionados = element.Solucionados;
          } else if (element.Pendientes) {
            pendientes = element.Pendientes;
          }
        });
        let result = [
          { name: 'Pendientes', value: pendientes },
          { name: 'Solucionados', value: solucionados },
        ];
        this.structureSingle = result;
      });
  }

  dataGraftInit() {
    this.requets.post('basicGraft', { area: this.Login.getArea() }).subscribe(
      (res: any) => {
        let pendientes;
        let solucionados;

        res.name.forEach((element) => {
          if (element.Solucionados) {
            solucionados = element.Solucionados;
          } else if (element.Pendientes) {
            pendientes = element.Pendientes;
          }
        });
        let result = [
          { name: 'Pendientes', value: pendientes },
          { name: 'Solucionados', value: solucionados },
        ];
        this.structureSingle = result;
      },
      (err) => {}
    );
  }

  onSelectionChange() {
    if (this.idreqst > 0) {
      this.test = this.testStatic.filter(
        (data) => data.idlasolicitud == this.idreqst
      );
    }
  }
  deletfilter() {
    this.idreqst = null;
    this.test = this.testStatic;
  }

  downLoadBill(doc) {
    window.open(environment.apiUrl + `/api/download?doc=${doc}`);
  }

  getAuth() {
    let dataRol = this.Login.getRol();
    this.authRol = dataRol.auth;
    this.Rol = dataRol.rol;

    /* alert(this.authRol + ' - ' + this.Rol); */
  }

  getInfoGeneral() {
    let dataArea = this.Login.getArea();
    this.requets.getInfo(dataArea).subscribe(
      (res: infoSelect) => {
        let AreaPerson = res.areaWorkers;
        AreaPerson.forEach((el) => {
          this.map.set(el.idccms, el.nameuser);
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getData() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    this.load = true;
    switch (this.Rol) {
      case 'admin':
        this.subcriptionRequest = this.requets
          .post('getRequest', { area: this.Login.getArea() })
          .subscribe(
            (res: dataRequest) => {
              this.load = false;
              this.test = res.Result;
              this.testStatic = res.Result;
            },
            (err) => {
              this.load = false;
            }
          );
        break;
      case 'dev':
        this.subcriptionRequest = this.requets
          .post('getRequestByResponsable', { idccms: this.Login.getIdccsms() })
          .subscribe(
            (res: dataRequest) => {
              this.load = false;
              this.test = res.Result;
              this.testStatic = res.Result;
            },
            (err) => {
              this.load = false;
            }
          );
        break;
      default:
        this.subcriptionRequest = this.requets
          .post('getRequestById', { idccms: this.Login.getIdccsms() })
          .subscribe(
            (res: dataRequest) => {
              this.load = false;
              this.test = res.Result;
              this.testStatic = res.Result;
            },
            (err) => {
              this.load = false;
            }
          );
        break;
    }
  }

  ngOnDestroy() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
  }
}
