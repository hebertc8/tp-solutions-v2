import { Component, OnInit } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { Subscription } from 'rxjs/internal/Subscription';
import { LoginService } from 'src/app/services/login.service';
import { RequestsService } from 'src/app/services/requests.service';

@Component({
  selector: 'app-dialog-confirmation',
  templateUrl: './dialog-confirmation.component.html',
  styleUrls: ['./dialog-confirmation.component.scss']
})
export class DialogConfirmationComponent implements OnInit {
  public subcriptionRequest: Subscription;
  email:string;
  constructor(protected dialogRef: NbDialogRef<DialogConfirmationComponent>, private Login: LoginService,private requets: RequestsService,private toastr: NbToastrService) { }

  message = '';
  ngOnInit(): void {}

  close(r) {
    this.dialogRef.close(r);
  }

  sendEmail(){
    this.subcriptionRequest = this.requets
    .post('updateEmail', {
      idccms: this.Login.getIdccsms(),
      email: this.email,
    })
    .subscribe(
      (res) => {
        this.toastr.success('Successful transfer', 'Success');
        let info =this.Login.getUser();
        info.email = this.email;
        this.Login.setUser(info);
        this.dialogRef.close();
      },
      (err) => {
      }
    );
  }
}
