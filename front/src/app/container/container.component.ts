import { Component, OnInit } from '@angular/core';
import { RequestsService } from '../services/requests.service';
import { infoSelect } from '../models/infoSelect';
import { GlobalConstants } from '../models/global';
import { LoginService } from '../services/login.service';
import { ChatService } from '../services/chat.service';
import { obschat } from 'src/app/services/obs-chat';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-container',
  template: `
    <nb-layout windowMode>
      <nb-layout-header fixed>
        <app-header></app-header>
      </nb-layout-header>
      <nb-sidebar class="menu-sidebar" tag="menu-sidebar" responsive>
        <nb-menu tag="menu" [items]="menu"></nb-menu>
      </nb-sidebar>
      <nb-layout-column>
        <router-outlet></router-outlet>
      </nb-layout-column>
      <nb-layout-footer fixed>
        <app-footer></app-footer>
      </nb-layout-footer>
    </nb-layout>
  `,
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent implements OnInit {
  menu: any = [];
  listMsj: any = [];
  title: any = document.title;
  numberUnreadMsg: number = 0;
  public suscriptor: Subscription;

  constructor(private requets: RequestsService, private Login: LoginService, private chat: ChatService, private obsChat: obschat) {

    chat.notifiEmit();
    chat.notificacion().subscribe((res: any) => {
      if (res == 1) {

      } else {
        this.unReadMessage(res);
      }
    });
  }

  ngOnInit() {
    this.getInfoGeneral();
    this.suscriptor = this.obsChat.datos$.subscribe(dato => {
      let num = this.menu.findIndex(element => element.title == 'Chat');
      this.menu[num] = {
        title: this.menu[num].title, link: this.menu[num].link, icon: this.menu[num].icon
      }
    });
    let menuAgent = [
      {
        title: 'In progress',
        link: '/container/progress',
        icon: 'briefcase',
      },
      {
        title: 'My historic',
        link: '/container/historic',
        icon: 'folder',
      },
      {
        title: 'Chat',
        link: '/container/chat',
        icon: 'message-circle-outline',
      },
    ];
    let menuAdmin = [
      {
        title: 'In progress',
        link: '/container/progress',
        icon: 'briefcase',
      },
      {
        title: 'My historic',
        link: '/container/historic',
        icon: 'folder',
      },
      {
        title: 'Reporting area',
        link: '/container/report',
        icon: 'activity-outline',
      },
      {
        title: 'Chat',
        link: '/container/chat',
        icon: 'message-circle-outline',
      },
      {
        title: 'Settings',
        link: '/container/settings',
        icon: 'settings-2-outline',
      }
    ];
    let menuSuperAdmin = [
      {
        title: 'In progress',
        link: '/container/progress',
        icon: 'briefcase',
      },
      {
        title: 'My historic',
        link: '/container/historic',
        icon: 'folder',
      },
      {
        title: 'Settings',
        link: '/container/settings',
        icon: 'settings-2-outline',
      },
      {
        title: 'Priority List',
        link: '/container/priority',
        icon: 'list-outline',
      },
      {
        title: 'Chat',
        link: '/container/chat',
        icon: 'message-circle-outline',
      },
    ];

    this.menu = this.Login.getUser().rol === 'superAdmin' ? menuSuperAdmin : this.Login.getUser().rol === 'admin' ? menuAdmin : menuAgent;
  }

  getInfoGeneral() {
    let dataArea = this.Login.getArea();
    this.requets.getInfo(dataArea).subscribe(
      (res: infoSelect) => {
        GlobalConstants.centrals = res.centrals;
        GlobalConstants.areas = res.project;
        GlobalConstants.cases = res.cases;
        GlobalConstants.areaWorkers = res.areaWorkers;
        GlobalConstants.subProject = res.subProject;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  pushNotifi() {
    if (!Notification) {
      alert('Las notificaciones de escritorio no estan disponibles en tu navegador.');
      return;
    } else if (Notification.permission === "granted") {
      var notification = new Notification("Hola", { icon: './assets/icons/tp/apple-icon.png', body: 'Tienes mas de 10 mensajes sin leer' });
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === "granted") {
          var notification = new Notification("Hola", { icon: './assets/icons/tp/apple-icon.png', body: 'Tienes mas de 10 mensajes sin leer' });
        }
      });
    }
  }

  unReadMessage(obj) {
    const audio = new Audio('assets/notify.wav');
    audio.play();
    if (!this.chat.getLobby()) {
      this.numberUnreadMsg++;
      let newTitle = '(' + this.numberUnreadMsg + ')' + this.title;
      let num = this.menu.findIndex(element => element.title == 'Chat');
      this.menu[num] = {
        title: this.menu[num].title, link: this.menu[num].link, icon: this.menu[num].icon, badge: {
          text: this.numberUnreadMsg,
          status: 'primary',
        },
      };
      document.title = newTitle;

      let aux = this.listMsj.findIndex(element => element.lobby == obj.lobby);
      if (aux >= 0) {
        this.listMsj[aux] = { lobby: obj.lobby, msj: (this.listMsj[aux].msj + 1) }
      } else {
        this.listMsj.push({ lobby: obj.lobby, msj: 1 });
      }
      this.chat.setlistMsj(this.listMsj);
    }

    if (this.numberUnreadMsg > 10) {
      this.pushNotifi();
    }
  }
}
