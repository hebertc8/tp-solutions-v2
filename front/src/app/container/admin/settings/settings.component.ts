import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { GlobalConstants } from 'src/app/models/global';
import { infoSelect } from 'src/app/models/infoSelect';
import { RequestsService } from 'src/app/services/requests.service';
import { LoginService } from '../../../services/login.service';
import { DialogConfirmationComponent } from '../../dialog-confirmation/dialog-confirmation.component';

interface Case {
  Id: number;
  Nombre?: string;
  Id_Project?: number;
  Project?: string;
  Id_Area?: number;
  Area?: string;
  Priority?: number;
  TimeFrame?: number;
  WorkDays?: boolean;
  Legend?: string;
  TimeValue?: number;
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  form: FormGroup;
  cases: Case[];
  global: any;
  areas= [{ id: null, name: 'loading..' }];
  subProject = [
    { Id_Project: null, Project: 'Loading', Id_Area: null, Area: '' },
  ];
  booleanProject: boolean = false;
  booleanspring: boolean=false;
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private dialog: NbDialogService,
    private request: RequestsService,
    private toastr: NbToastrService
  ) {
    this.form = fb.group({
      area: [null],
      priority: [null, Validators.required],
      project: [''],
      case: ['', Validators.required],
      dataTime: [''],
      time: ['', [Validators.min(0), Validators.max(60)]],
      timeType: [''],
    });
  }
  ngOnInit(): void {
    this.getInfoGeneral();
  }

  valideCase() {
    let res = this.form.get('case').value;
    let cond = this.cases.find(
      (data) => data.Nombre.toUpperCase() == res.toUpperCase()
    );
    if (cond) {
      return false;
    } else {
      return true;
    }
  }

  addCase(event: Event) {
      event.preventDefault();
      if (this.form.valid) {
        this.getarea();
        const input = this.form.value;
        this.request.post('addCase', input).subscribe(
          (res: Case[]) => {
            GlobalConstants.cases = res;
            this.getInfoGeneral();
            this.form.reset();
          },
          (err) => {}
        );
      }
  }
  getarea() {
    let areadata = this.areas.find(
      (d) => d.name == this.loginService.getArea()
    );
    this.form.controls['area'].setValue(areadata.id);
    let timestring;
    if (this.form.get('timeType').value == '1') {
      timestring = 'Minute';
    } else if (this.form.get('timeType').value == '2') {
      timestring = 'Hour';
    } else {
      timestring = 'Day';
    }
    if (this.form.get('time').value > 1) {
      this.form.controls['dataTime'].setValue(
        this.form.get('time').value + ' ' + timestring + 's'
      );
    } else {
      this.form.controls['dataTime'].setValue(
        this.form.get('time').value + ' ' + timestring
      );
    }
  }
  deleteCase(_id) {
    const dialog = this.dialog
      .open(DialogConfirmationComponent, {
        closeOnBackdropClick: false,
        closeOnEsc: false,
        autoFocus: false,
        context: {
          message: 'Are you sure you want to cancel this case?',
        },
      })
      .onClose.subscribe((resp) => {
        if (resp) {
          this.request.post('deleteCase', { id: _id }).subscribe(
            (res: Case[]) => {
              GlobalConstants.cases = res;
              this.filtreData(res);
              this.getInfoGeneral()
            },
            (err) => {}
          );
        }
      });
  }

  filtreData(data: Case[]) {
    this.cases = data.filter(
      (data) => data.Area == this.loginService.getArea()
    );
  }

  getInfoGeneral() {
    this.booleanspring=true;
    let dataArea = this.loginService.getArea();
    this.request.getInfo(dataArea).subscribe(
      (res: infoSelect) => {
        this.booleanspring=false;
        GlobalConstants.cases = res.cases;
        this.areas = res.project;
        this.subProject = GlobalConstants.subProject.filter(
          (d) => d.Area == this.loginService.getArea()
        );
        let globalData = this.subProject.find((d) => d.Project == 'Global');
        this.global = globalData.Id_Project;
        this.subProject = this.subProject.filter((d) => d.Project != 'Global');

        if (this.subProject.length > 0) {
          this.booleanProject = true;
        } else {
          this.booleanProject = false;

          this.form.controls['project'].setValue(this.global);
        }

        this.filtreData(res.cases);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
