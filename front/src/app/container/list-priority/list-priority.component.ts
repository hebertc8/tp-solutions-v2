import { Component, OnInit } from '@angular/core';
import {
  NbComponentStatus,
  NbDialogService,
  NbGlobalLogicalPosition,
  NbGlobalPosition,
  NbToastrService,
} from '@nebular/theme';
import { LocalDataSource } from 'ng2-smart-table';
import { NbDateService } from '@nebular/theme';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { DialogConfirmationComponent } from '../dialog-confirmation/dialog-confirmation.component';
import { RequestsService } from 'src/app/services/requests.service';

export interface task {
  _id: string;
  task: string;
  description: string;
  priority: number;
  dataTime: Date;
}
@Component({
  selector: 'app-list-priority',
  templateUrl: './list-priority.component.html',
  styleUrls: ['./list-priority.component.scss'],
})
export class ListPriorityComponent implements OnInit {
  settings = {
    mode: 'external',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      position: 'right',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-check iconSize"></i>',
    },
    attr: {
      class: 'tablePrimary',
    },
    columns: {
      priority: {
        title: 'Prioridad',
        filter: false,
        editable: false,
      },
      task: {
        title: 'Tarea',
        filter: false,
        editable: false,
      },
      description: {
        title: 'Descripcion',
        filter: false,
        editable: false,
      },
      dataTime: {
        title: 'Fecha estimada',
        filter: false,
        editable: false,
        valuePrepareFunction: (dataTime) => {
          var formatted = moment(dataTime).utc().format('L');
          return formatted;
        },
      },
    },
  };
  settings2 = {
    mode: 'external',
    hideSubHeader: true,
    actions: {
      add: false,
      edit: false,
      delete: false,
      position: 'right',
    },
    attr: {
      class: 'tablePrimary',
    },
    columns: {
      priority: {
        title: 'Prioridad',
        filter: false,
        editable: false,
      },
      task: {
        title: 'Tarea',
        filter: false,
        editable: false,
      },
      description: {
        title: 'Descripcion',
        filter: false,
        editable: false,
      },
      dataTime: {
        title: 'Fecha estimada',
        filter: false,
        editable: false,
        valuePrepareFunction: (dataTime) => {
          var formatted = moment(dataTime).utc().format('L');
          return formatted;
        },
      },
    },
  };
  newBoolean: boolean = true;
  source: task[];
  source2: task[];
  min: Date;
  form: FormGroup;

  constructor(
    private toastrService: NbToastrService,
    protected dateService: NbDateService<Date>,
    private dialog: NbDialogService,
    private login: LoginService,
    private request: RequestsService,
    private fb: FormBuilder
  ) {
    this.min = this.dateService.addDay(this.dateService.today(), 0);
    this.getTask();
    this.getfinishTask()
    this.form = fb.group({
      dataTime: [Date],
      task: ['', Validators.required],
      description: ['', Validators.required],
      priority: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  newTask() {
    this.newBoolean = !this.newBoolean;
    this.form.reset();
  }

  getTask() {
    this.request.get('getTask').subscribe(
      (res: task[]) => {
        this.source = res;
        this.form.reset();
      },
      (err) => {}
    );
  }

  submit(event) {
    const input = this.form.value;
    this.request.post('addtask', input).subscribe(
      (res: task[]) => {

        this.newTask();
        this.getTask();
      },
      (err) => {
        console.log(err);
      }
    );
  }
  getfinishTask() {
    this.request.get('getfinishTask').subscribe(
      (res: task[]) => {
        this.source2 = res;
      },
      (err) => {}
    );
  }
  changeTab(event){
    if (event.tabTitle== 'History') {
      this.getfinishTask();
    }
  }
  deleteAction(event) {
    let status: NbComponentStatus;
    let position: NbGlobalPosition = NbGlobalLogicalPosition.BOTTOM_END;
    //mejorar pestaña de confirmacion
    const dialog = this.dialog
      .open(DialogConfirmationComponent, {
        closeOnBackdropClick: false,
        closeOnEsc: false,
        autoFocus: false,
        context: {
          message: `Finalizar la tarea ${event.data.task}?`,
        },
      })
      .onClose.subscribe((resp) => {
        if (resp) {
          this.request.post('finishTask', { _id: event.data._id }).subscribe(
            (res) => {
              this.getTask();
            },
            (err) => {}
          );
        }
      });
  }
}
