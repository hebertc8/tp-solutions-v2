import { Component, OnInit, OnDestroy } from '@angular/core';
import { RequestsService } from 'src/app/services/requests.service';
import { dataRequest } from 'src/app/models/request';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.component.html',
  styleUrls: ['./bitacora.component.scss']
})
export class BitacoraComponent implements OnInit,OnDestroy {

  public test: dataRequest;
  public subcriptionRequest: Subscription;
  closed = false;
  requestUpdate = 0;
  decriptionClosed='';
  load = false;


  constructor(private requets: RequestsService) {}

  ngOnInit(): void {
    this.getData();

  }

  getData(){
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
    this.load = true;
    this.subcriptionRequest= this.requets.get('getRequest').subscribe((res:dataRequest) => {
      this.load = false;
      this.test = res;
    }, err => {
      this.load = false;
    });
  }
  ngOnDestroy() {
    if (this.subcriptionRequest) {
      this.subcriptionRequest.unsubscribe();
    }
  }


}
