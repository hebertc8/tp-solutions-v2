import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ContainerComponent } from './container/container.component';
import { MainComponent } from './container/main/main.component';
import { LoginGuard } from './services/guards/login.guard';
import { ValidateLoginGuard } from './services/guards/validate-login.guard';
import { ProgressComponent } from './container/progress/progress.component';
import { HistoricComponent } from './container/historic/historic.component';
import { BitacoraComponent } from './container/bitacora/bitacora.component';
import { SettingsComponent } from './container/admin/settings/settings.component';
import { AdminUserGuard } from './services/guards/admin-user.guard';
import { ListPriorityComponent } from './container/list-priority/list-priority.component';
import { LobbyComponent } from './container/lobby/lobby.component';
import { AreaReportingComponent } from './container/area-reporting/area-reporting.component';
import { SurveyComponent } from './container/survey/survey.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'prefix' },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuard],
  },
  {
    path: 'survey/:id',
    component: SurveyComponent
  },

  {
    path: 'container',
    component: ContainerComponent,
    children: [
      {
        path: 'historic',
        component: HistoricComponent,
        canActivate: [ValidateLoginGuard],
      },
      {
        path: 'progress',
        component: ProgressComponent,
        canActivate: [ValidateLoginGuard],
      },
      {
        path: 'bitacora',
        component: BitacoraComponent,
        canActivate: [ValidateLoginGuard],
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [ValidateLoginGuard,AdminUserGuard],
      },
      {
        path: 'priority',
        component: ListPriorityComponent,
        canActivate: [ValidateLoginGuard,AdminUserGuard],
      },
      {
        path: 'chat',
        component: LobbyComponent,
        // canActivate: [ValidateLoginGuard],
      },
      {
        path: 'report',
        component: AreaReportingComponent,
        canActivate: [ValidateLoginGuard,AdminUserGuard],
      },
    ],
  },
  { path: '**', redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
